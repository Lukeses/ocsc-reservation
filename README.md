# README

**Twin Rocks - Property Booking Application (Ocean Coast Shipping Corp)**


* Ruby version - 2.3.0 //use rbenv

* Rails version - 5.0.2

* Database - mysql (mysql2 gem)

* Create Database By Running : 
```
#!ruby
rails db:create
```


* FrontEnd - PaperDashboard (Licensed)

* Jquery Ajax Json

**Create Directory for Cargo System in: **


1. /var/**ocsc-websystemapps**/ocsc-reservation/


2. Then change the directory *ocsc-websystemapps* to be writable by running:

```
#!linux
sudo chmod 777 ocsc-websystemapps
```


### Set-Up: ###
1. [Install Ruby On Rails (Ubuntu)](https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-16-04)


2. [Install MYSQL (Ubuntu)](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-16-04)


3. [Required Initial Configuration on MYSQL for Cargo](https://bitbucket.org/Lukeses/ocsc-tms/issues/12/soa-query-only_full_group)

4. Install expect, run this: 
```
#!linux
sudo apt-get install expect
```
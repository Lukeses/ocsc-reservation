class Actions extends React.Component {

  constructor(props){
    super(props)
    this.edit = this.edit.bind(this)
    this.delete = this.delete.bind(this)
  }

  edit(){
    const pass = this.props
    this.props.editAction(pass.id, pass.index)
  }

  delete(){
    const pass = this.props
    this.props.deleteAction(pass.id, pass.index)
  }

  renderMain(){
    var icons = {
      edit: "ti-pencil",
      delete: "ti-close"
    }
    return(
			<div className="table-icons">
				<button rel="tooltip"
					 className="btn btn-simple btn-info btn-warning table-action"
           onClick={this.edit}
					 data-original-title="Edit" >
					<i className={icons.edit}></i>
				</button>
				<button rel="tooltip"
					 className="btn btn-simple btn-danger btn-icon table-action"
           onClick={this.delete}
					 data-original-title="Delete">
					<i className={icons.delete}></i>
				</button>
			</div>
    )
  }

  render () {
    return this.renderMain()
  }
}

class AddNewButton extends React.Component {
  constructor(props){
    super(props)
    this.add_new = this.add_new.bind(this)
  }
  add_new(){
    this.props.add_new()
  }
  renderMain(){
    return(
			<div className="row">
				<div className="col-sm-2 col-xs-12 form-group pull-right">
					<button className="btn btn-fill btn-info btn-lg btn-magnify"
            onClick={this.add_new}>
						<i className="ti-plus"></i> Add New
					</button>
				</div>
			</div>
    )
  }
  render () {
    return this.renderMain()
  }
}

class Clients extends React.Component {

	constructor(props){
		super(props)
		this.getAjaxRecords = this.getAjaxRecords.bind(this)
    this.new = this.new.bind(this)
    this.fetch = this.fetch.bind(this)
    this.delete = this.delete.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.handleSearch = this.handleSearch.bind(this)
		this.save = this.save.bind(this)
		this.update = this.update.bind(this)
		this.clear = this.clear.bind(this)
    this.modal = {
      id: "modal_clients",
      size: "modal-md",
      icon: "",
      title: "Client / Guest"
    }
		this.state = {
			records:  [],
			mode: null,
      name: '',
      company: '',
      contact: '',
      email: ''
		}
	}

	loadOldScripts(){
		uc.sidebar.menu_name1(`clients`)
	}

	componentDidMount(){
		this.loadOldScripts()
		this.getAjaxRecords()
	}

	getAjaxRecords(){
		self = this
		$.ajax({
			url: `/clients/records`,
			type: "POST",
			success: (s)=>{
				self.setState({records: s})
			},
			error: (xhr, status, error)=>{
				console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
			}
		})
	}

  save(callback){
		self = this
		const data = {
      name: this.state.name,
      company: this.state.company,
      contact: this.state.contact,
      email: this.state.email
		}
		//validate
		if(this.validate(this.state)){
			$.ajax({
				url: `/clients`,
				data: data,
				type: "POST",
				success: (s)=>{
					switch(s.status){
						case "success":
							self.clear()
							self.getAjaxRecords()
							toastr.success(`New Client/Guest`,`Saved`)
							swal({
								title: "Saved",
								text: "New Client/Guest has been saved",
								type: "success",
								allowEscapeKey: false,
								allowOutsideClick: false
							}, ()=>{
									// do something after clicking ok of swal
							})
						break
						case "duplicate":
							toastr.warning(s.message, s.status.toUpperCase())
							// this.refs.btn_save.disabled = false
						break
					}
				},
				error: (xhr, status, error)=>{
					toastr.error(`Something Happended`,`Error`)
					console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
					// this.refs.btn_save.disabled = false
				}
			})

		}else{
			toastr.error('Please Fill Out Required Fields. Labels with *',`Incomplete Fields`)
		}
		callback()
  }

  update(id,callback){
    self = this
		const data = {
      name: this.state.name,
      company: this.state.company,
      contact: this.state.contact,
      email: this.state.email
		}
		if(this.validate(this.state)){
			$.ajax({
				url: `/clients/${id}`,
				data: data,
				type: "patch",
				success: (s)=>{
					switch(s.status){
						case "success":
							self.setState({mode: "save"})
							self.clear()
							self.getAjaxRecords()
							toastr.success(`New client/guest`,`Updated`)
							swal({
								title: "Updated",
								text: "Client/guest has been Updated",
								type: "success",
								allowescapekey: false,
								allowoutsideclick: false
							}, ()=>{
									// do something after clicking ok of swal
							})
						break
						case "duplicate":
							toastr.warning(s.message, s.status.toUpperCase())
							// this.refs.btn_save.disabled = false
						break
					}
				},
				error: (xhr, status, error)=>{
					toastr.error(`something happended`,`error`)
					console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
					// this.refs.btn_save.disabled = false
				}
			})

		}else{
			toastr.error('please fill out required fields. labels with *',`incomplete fields`)
		}
		callback()
  }

	validate(f){
		let v = true
		//specific validation adding has-error to div wrapper of element
		//name
		if(f.name==''){
			$('#d_name').addClass('has-error')
			v = false
		}else{$('#d_name').removeClass('has-error')}

    //return validation result
		return v
	}

	new(){
		this.clear()
		this.setState({mode: "save"},()=>{
			this.showModal()
		})
	}

	handleChange(e){
		const value = e.target.value
		const name = e.target.name
		this.setState({[name]: value},()=>{
			// console.log(this.state[name])
		})
	}

	handleSearch(s){
		this.setState({records: s})
	}

	clear(){
		this.setState({
      name: '',
      company: '',
      contact: '',
      email: ''
		},()=>{
			$('#d_name').removeClass('has-error')
		})

	}

  fetch(id, index){
		self = this
		this.clear()
		this.setState({mode: id},()=>{
			self.setState({
				name: this.state.records[index].name,
				company: this.state.records[index].company,
				contact: this.state.records[index].contact,
				email: this.state.records[index].email
			},()=>{
				this.showModal()
			})
		})
  }

	showModal(){
		$(`#${this.modal.id}`).modal('show')
	}

  delete(id, index){
    self = this
    let newRecords = this.state.records
    //confirmation
    swal({
      title: 'Are you sure?',
      text: "You are delete this record!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Delete'
    }).then(function () {
			$.ajax({
				url: `/clients/${id}`,
				type: "DELETE",
				success: (s)=>{
					switch(s.status){
						case "success":
				      newRecords.splice(index,1)
				      self.setState({records: newRecords},()=>{
				        toastr.info(`Record successfully removed`,`Deleted`)
				      })
						break
					}
				},
				error: (xhr, status, error)=>{
					console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
				}
			})
    })
  }

  renderMain(){
		//for ClientsForm
		var fields = {
      name: this.state.name,
      company: this.state.company,
      contact: this.state.contact,
      email: this.state.email
		}
    return(
			<div>
			<ClientsForm modal={this.modal}
				fields={fields}
				update={this.update}
				save={this.save}
				change={this.handleChange}
				mode={this.state.mode}/>
			<AddNewButton add_new={this.new}/>
      <Container>
				<TableSearch searched={this.handleSearch}
					url="clients/search"/>
				<Table data={this.state.records}
					thead={['Name','Company','Contact','Email']}
					tdata={[
						{column: 'name'},
						{column: 'company'},
						{column: 'contact'},
						{column: 'email'}
					]}
					classes="table-hover table-striped"
					editAction={this.fetch}
					deleteAction={this.delete}/>
      </Container>
			</div>
    )
  }

  render () {
    return this.renderMain()
  }
}

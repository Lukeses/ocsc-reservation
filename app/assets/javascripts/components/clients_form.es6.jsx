class ClientsForm extends React.Component {
  constructor(props){
    super(props)
    this.renderMain = this.renderMain.bind(this)
  }

  renderMain(){
    return(
      <Modal modal={this.props.modal}>
        <form  id="form_clients">
          <div className="row">
            <div className="col-sm-12">
							<div className="form-group col-xs-12"
                id="d_name">
								<label className="control-label">Name<star>*</star></label>
								<input className="form-control"
											 onChange={this.props.change}
                       value={this.props.fields.name}
											 name="name"
                       title="Input Full Name"
											 required="true"/>
							</div>
							<div className="form-group col-xs-12"
                id="d_company">
								<label className="control-label">Company</label>
								<input className="form-control"
                       value={this.props.fields.company}
											 onChange={this.props.change}
                       placeholder="(Optional)"
											 name="company" />
							</div>
							<div className="form-group col-xs-12"
                id="d_contact">
								<label className="control-label">Contact</label>
								<input className="form-control"
                       value={this.props.fields.contact}
											 onChange={this.props.change}
                       title="Phone or Tel. No"
                       placeholder="(Optional)"
                       type="number"
											 name="contact" />
							</div>
							<div className="form-group col-xs-12"
                id="d_email">
								<label className="control-label">Email Address</label>
								<input className="form-control"
                       value={this.props.fields.email}
											 onChange={this.props.change}
                       placeholder="(Optional)"
											 name="email" />
							</div>
            </div>
          </div>
          <ModalSaveUpdate mode={this.props.mode}
            update={this.props.update}
            save={this.props.save}/>
        </form>
      </Modal>
    )
  }

  render () {
    return this.renderMain()
  }
}

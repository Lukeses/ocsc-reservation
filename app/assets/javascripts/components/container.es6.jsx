class Container extends React.Component {
  render () {
    return(
			<div className="row">
				<div className="col-md-12">
					<div className="card">
						<div className="card-content">
              {this.props.children}
						</div>
					</div>
				</div>
			</div>
    )
  }
}

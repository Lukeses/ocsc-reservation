class DiscountsForm extends React.Component {
  constructor(props){
    super(props)
    this.renderMain = this.renderMain.bind(this)
  }

  renderMain(){
    return(
      <Modal modal={this.props.modal}>
        <form  id="form_discounts">
          <div className="row">
            <div className="col-sm-12">
							<div className="form-group col-xs-12"
                id="d_name">
								<label className="control-label">Name<star>*</star></label>
								<input className="form-control"
											 onChange={this.props.change}
                       value={this.props.fields.name}
											 name="name"
                       title="Input Full Name"
											 required="true"/>
							</div>
							<div className="form-group col-xs-12"
                id="d_description">
								<label className="control-label">Description</label>
								<textarea className="form-control"
											 onChange={this.props.change}
                       value={this.props.fields.description}
											 name="description" />
							</div>
							<div className="form-group col-sm-6 col-xs-12"
                id="d_discount_type">
								<label className="control-label">Type<star>*</star></label>
								<select className="form-control"
											 name="discount_type"
                       required >
                  <option value="percentage">Percentage</option>
                </select>
							</div>
							<div className="form-group col-sm-6 col-xs-12"
                id="d_amount">
								<label className="control-label">Amount<star>*</star></label>
								<input className="form-control"
											 onChange={this.props.change}
                       value={this.props.fields.amount}
                       type="number"
											 name="amount"
                       required />
							</div>
            </div>
          </div>
          <ModalSaveUpdate mode={this.props.mode}
            update={this.props.update}
            save={this.props.save}/>
        </form>
      </Modal>
    )
  }

  render () {
    return this.renderMain()
  }
}

class EmptyTable extends React.Component {
  constructor(props){
    super(props)
    this.renderMain = this.renderMain.bind(this)
  }

  renderMain(){
		return(
			<tr>
				<td colSpan={this.props.colspan}>
					Empty Data
				</td>
			</tr>
		)
  }

  render () {
    return this.renderMain()
  }
}

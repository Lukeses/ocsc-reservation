const Filter = (props) => {
  return( 
    <div>
      <div className="col-sm-4">
        <button 
        onClick={props.filter}
        className="btn btn-primary">
          <i className="ti-filter"></i> Filter
        </button>
      </div>
    </div>
  )
}


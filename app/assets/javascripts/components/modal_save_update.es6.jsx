class ModalSaveUpdate extends React.Component {
  constructor(props){
    super(props)
    this.renderSaveUpdate = this.renderSaveUpdate.bind(this)
    this.renderMain = this.renderMain.bind(this)
    this.update = this.update.bind(this)
    this.save = this.save.bind(this)
  }
  update(){
    document.getElementById('modal-update').disabled = true
    this.props.update(this.props.mode, ()=>{
      document.getElementById('modal-update').disabled = false
    })
  }
  save(){
    document.getElementById('modal-save').disabled = true
    this.props.save(()=>{
      document.getElementById('modal-save').disabled = false
    })
  }
  renderSaveUpdate(){
    if(this.props.mode == "save"){
      return(
        <button type="button"
          onClick={this.save}
          id="modal-save"
          className="btn btn-info btn btn-fill btn-lg">
          SAVE
        </button>
      )
    }
    else{
      return(
        <div>
        <button type="button"
          onClick={this.update}
          id="modal-update"
          className="btn btn-warning btn btn-fill btn-lg">
          UPDATE
        </button>
        </div>
      )
    }
  }
  renderMain(){
    return(
      <div>
        <hr/>
        <div className="text-center">
          {this.renderSaveUpdate()}
        </div>
        <div className="clearfix"></div>
      </div>
    )
  }
  render () {
    return this.renderMain()
  }
}

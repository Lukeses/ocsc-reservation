class NewReservation extends React.Component {
  loadOldStartupScripts(){
    uc.sidebar.menu_name1(`reservations_new`)
  }
  componentDidMount(){
    this.loadOldStartupScripts()
  }
  render () {
    const dropdowns = {
      sources: this.props.sources,
      transportation: this.props.transportation,
      discount: this.props.discount,
      property: this.props.property,
			clients: this.props.clients,
      rental_service: this.props.rental_service
    }
    return(
      <ReservationForm dropdowns={dropdowns} 
											 reserve={this.props.reserve}/>
    )
  }
}

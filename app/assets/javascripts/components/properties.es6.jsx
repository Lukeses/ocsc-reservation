class Properties extends React.Component {

	constructor(props){
		super(props)
    this.renderMain = this.renderMain.bind(this)
		this.handleSearch = this.handleSearch.bind(this)
    this.getAjaxRecords = this.getAjaxRecords.bind(this)
		this.new = this.new.bind(this)
		this.delete = this.delete.bind(this)
		this.fetch = this.fetch.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.save = this.save.bind(this)
		this.update = this.update.bind(this)
		this.getFields = this.getFields.bind(this)
    this.modal = {
      id: "modal_properties",
      size: "modal-md",
      icon: "",
      title: "Properties"
    }
		this.state = {
			records:  [],
			mode: null,
			name: '',
			rate: '',
      property_type: 'none',
      capacity: ''
		}
	}

	loadOldScripts(){
		uc.sidebar.menu_name1('properties')
	}

	componentDidMount(){
		this.loadOldScripts()
		this.getAjaxRecords()
	}

	getAjaxRecords(){
		self = this
		$.ajax({
			url: `/properties/records`,
			type: "POST",
			success: (s)=>{
				self.setState({records: s})
			},
			error: (xhr, status, error)=>{
				console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
			}
		})
	}

	getFields(){
		return({
			name: this.state.name,
			rate: this.state.rate,
			property_type: this.state.property_type,
			capacity: this.state.capacity
		})
	}

	handleSearch(s){
		this.setState({records: s})
	}

	handleChange(e){
		const value = e.target.value
		const name = e.target.name
		this.setState({[name]: value},()=>{
			// console.log(this.state[name])
		})
	}

	showModal(){
		$(`#${this.modal.id}`).modal('show')
	}

	validate(f){
		let v = true
		//specific validation adding has-error to div wrapper of element
		//name
		if(f.name==''){
			$('#d_name').addClass('has-error')
			v = false
		}else{$('#d_name').removeClass('has-error')}
		//amount
		if(f.rate=='' || f.rate<=0 || f.rate>=90000){
			$('#d_rate').addClass('has-error')
			v = false
		}else{$('#d_rate').removeClass('has-error')}
		//property_type
		if(f.property_type=='none'){
			$('#d_property_type').addClass('has-error')
			v = false
		}else{$('#d_property_type').removeClass('has-error')}
		//capactiy
		if(f.capacity=='' || f.capacity<=0 || f.capacity>=100){
			$('#d_capacity').addClass('has-error')
			v = false
		}else{$('#d_capacity').removeClass('has-error')}
    //return validation result
		return v
	}

  save(callback){
		self = this
		const data = this.getFields()
		//validate
		if(this.validate(this.state)){
			$.ajax({
				url: `/properties`,
				data: data,
				type: "POST",
				success: (s)=>{
					switch(s.status){
						case "success":
							self.clear()
							self.getAjaxRecords()
							toastr.success(`New Discount`,`Saved`)
							swal({
								title: "Saved",
								text: "New Property has been saved",
								type: "success",
								allowEscapeKey: false,
								allowOutsideClick: false
							}, ()=>{
									// do something after clicking ok of swal
							})
						break
						case "duplicate":
							toastr.warning(s.message, s.status.toUpperCase())
							// this.refs.btn_save.disabled = false
						break
					}
				},
				error: (xhr, status, error)=>{
					toastr.error(`Something Happended`,`Error`)
					console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
					// this.refs.btn_save.disabled = false
				}
			})

		}else{
			toastr.error('Please Fill Out Required Fields. Labels with *',`Incomplete Fields`)
		}
		callback()
  }

  update(id,callback){
    self = this
		const data = this.getFields()
		if(this.validate(this.state)){
			$.ajax({
				url: `/properties/${id}`,
				data: data,
				type: "patch",
				success: (s)=>{
					switch(s.status){
						case "success":
							self.setState({mode: "save"})
							self.clear()
							self.getAjaxRecords()
							toastr.success(`New Property`,`Updated`)
							swal({
								title: "Updated",
								text: "Property has been Updated",
								type: "success",
								allowescapekey: false,
								allowoutsideclick: false
							}, ()=>{
									// do something after clicking ok of swal
							})
						break
						case "duplicate":
							toastr.warning(s.message, s.status.toUpperCase())
							// this.refs.btn_save.disabled = false
						break
					}
				},
				error: (xhr, status, error)=>{
					toastr.error(`something happended`,`error`)
					console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
					// this.refs.btn_save.disabled = false
				}
			})

		}else{
			toastr.error('please fill out required fields. labels with *',`incomplete fields`)
		}
		callback()
  }

	new(){
		this.clear()
		this.setState({mode: "save"},()=>{
			this.showModal()
		})
	}

  fetch(id, index){
		self = this
		this.clear()
		this.setState({mode: id},()=>{
			self.setState({
				name: this.state.records[index].name,
				rate: this.state.records[index].rate,
				property_type: this.state.records[index].property_type,
				capacity: this.state.records[index].capacity
			},()=>{
				this.showModal()
			})
		})
  }

	clear(){
		this.setState({
			name: '',
			rate: '',
			property_type: 'none',
			capacity: ''
		},()=>{
			$('#d_name').removeClass('has-error')
			$('#d_rate').removeClass('has-error')
			$('#d_property_type').removeClass('has-error')
			$('#d_capacity').removeClass('has-error')
		})
	}

  delete(id, index){
    self = this
    let newRecords = this.state.records
    //confirmation
    swal({
      title: 'Are you sure?',
      text: "You are delete this record!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Delete'
    }).then(function () {
			$.ajax({
				url: `/properties/${id}`,
				type: "DELETE",
				success: (s)=>{
					switch(s.status){
						case "success":
				      newRecords.splice(index,1)
				      self.setState({records: newRecords},()=>{
				        toastr.info(`Record successfully removed`,`Deleted`)
				      })
						break
					}
				},
				error: (xhr, status, error)=>{
					console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
				}
			})
    })
  }

  renderMain(){
		const fields = this.getFields()
    return(
      <div>
				<PropertiesForm fields={fields}
					change={this.handleChange}
					save={this.save}
					update={this.update}
					modal={this.modal}
					mode={this.state.mode}/>
				<AddNewButton add_new={this.new}/>
        <Container>
					<TableSearch searched={this.handleSearch}
						url="properties/search"/>
          <Table data={this.state.records}
            thead={['Name','Rate','Type','Capacity']}
            tdata={[
							{column: 'name'},
							{column: 'rate', transform: 'peso'},
              {column: 'property_type', transform: 'capitalize'},
              {column: 'capacity', transform: 'fixed0'}
						]}
						deleteAction={this.delete}
						editAction={this.fetch}
            classes="table-hover table-striped" />
        </Container>
      </div>
    )
  }

  render () {
    return this.renderMain()
  }
}

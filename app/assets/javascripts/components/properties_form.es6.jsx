class PropertiesForm extends React.Component {
  constructor(props){
    super(props)
    this.renderMain = this.renderMain.bind(this)
  }

  renderMain(){
    return(
      <Modal modal={this.props.modal}>
        <form  id="form_properties">
          <div className="row">
            <div className="col-sm-12">
							<div className="form-group col-sm-8"
                id="d_name">
								<label className="control-label">Name<star>*</star></label>
								<input className="form-control"
											 onChange={this.props.change}
                       value={this.props.fields.name}
											 name="name"
                       title="Input Property Name / Description"
											 required="true"/>
							</div>
							<div className="form-group col-sm-4"
                id="d_rate">
								<label className="control-label">Rate<star>*</star></label>
								<input className="form-control"
											 onChange={this.props.change}
                       value={this.props.fields.rate}
                       type="number"
											 name="rate"
											 required="true"/>
							</div>
							<div className="form-group col-sm-6"
                id="d_property_type">
								<label className="control-label">Type<star>*</star></label>
								<select className="form-control"
											 onChange={this.props.change}
                       value={this.props.fields.property_type}
											 name="property_type"
											 required="true">
                  <option value="none">Select Type</option>
                  <option value="room">Room</option>
                  <option value="cottage">Cottage</option>
                  <option value="event">Event</option>
                </select>
							</div>
							<div className="form-group col-sm-6"
                id="d_capacity">
								<label className="control-label">Capacity<star>*</star></label>
								<input className="form-control"
											 onChange={this.props.change}
                       value={this.props.fields.capacity}
                       type="number"
											 name="capacity"
											 required="true"/>
							</div>


            </div>
          </div>
          <ModalSaveUpdate mode={this.props.mode}
            update={this.props.update}
            save={this.props.save}/>
        </form>
      </Modal>
    )
  }

  render () {
    return this.renderMain()
  }
}

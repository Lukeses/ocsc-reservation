class QuickRentalServices extends React.Component {
  constructor(props){
    super(props)
    this.clear = this.clear.bind(this)
    this.handleSave = this.handleSave.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.renderMain = this.renderMain.bind(this)
    this.modal = {
      id: "modal_rental_services",
      size: "modal-md",
      icon: "",
      title: "Rental Service"
    }
    this.state = {
      name: '',
      rate: ''
    }
  }


	handleChange(e){
		const value = e.target.value
		const name = e.target.name
		this.setState({[name]: value},()=>{
			// console.log(this.state[name])
		})
	}

  handleSave(){
    if(this.validate(this.state)){
      self = this
      const data = {
        description: this.state.name,
        rate: this.state.rate
      }
      this.refs.btn_save.disabled = true
      $.ajax({
        url: `/rental_services`,
        data: data,
        type: "POST",
        success: (s)=>{
          switch(s.status){
            case "success":
              toastr.success(`New Reservation`,`Saved`)
              self.refs.btn_save.disabled = false
              $(`#${self.modal.id}`).modal('hide')
              self.clear()
              self.props.append_rental_service(s.id, s.text)
            break

            case "error":
              for(let err in s.error){
                s.error[err].map((message)=>{
                  toastr.warning(message,err.toUpperCase())
                })
                self.refs.btn_save.disabled = false
              }
            break
          }
        },
        error: (xhr, status, error)=>{
          toastr.error(`Something Happended`,`Error`)
          console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
          self.refs.btn_save.disabled = false
        }
      })
    }
    return false
  }
  clear(){
    this.setState({ 
      name: '',
      rate: ''
    })
    $('#d_q_name #d_q_rate').removeClass('has-error')
  }
	validate(f){
		let v = true
		//specific validation adding has-error to div wrapper of element
		//name
		if(f.name==''){
			$('#d_q_name').addClass('has-error')
			v = false
		}else{$('#d_q_name').removeClass('has-error')}
		//amount
		if(f.rate=='' || f.rate<=0 || f.rate>=90000){
			$('#d_q_rate').addClass('has-error')
			v = false
		}else{$('#d_q_rate').removeClass('has-error')}
    //return validation result
		return v
	}


  renderMain(){
    return(
      <div>
        <Modal modal={this.modal}>
          <div className="row">
            <div className="col-sm-12">
              <div className="form-group col-sm-8" id="d_q_name">
                <label className="control-label">Name<star>*</star></label>
                <input className="form-control" 
                   onChange={this.handleChange}
                   value={this.state.name}
                  name="name" 
                  required="true"/>
              </div>
              <div className="form-group col-sm-4" id="d_q_rate">
                <label className="control-label">Rate<star>*</star></label>
                <input className="form-control"
                   onChange={this.handleChange}
                   value={this.state.rate}
                   type="number"
                   name="rate"
                   required />
              </div>
            </div>
          </div>
            <hr/>
            <div className="text-center">
              <button 
                ref="btn_save"
                onClick={this.handleSave}
                className="btn btn-fill btn-info">
                <i className="ti-plus"></i> ADD
              </button>
            </div>
        </Modal>
      </div>
    )
  }

  render () {
    return this.renderMain()
  }
}


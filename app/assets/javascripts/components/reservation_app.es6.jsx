class ReservationApp extends React.Component {
	constructor(props){
		super(props)
		const d = new Date()
		const monthName = this.getMonthNames()
		const days = new Date(d.getFullYear(),(parseInt(d.getMonth())+1),0)
		this.updateDate = this.updateDate.bind(this)
		this.populateProperties = this.populateProperties.bind(this)
		this.selectedCell = this.selectedCell.bind(this)
		this.state = {
			date: {
				month: d.getMonth(),
				year: d.getFullYear(),
				monthName: monthName[d.getMonth()],
				days: days.getDate()
			},
			properties: this.populateProperties(days.getDate())
		}
	}

	updateDate(m,y){
		const monthName=this.getMonthNames()
		const days = new Date(y,(parseInt(m)+1),0)
		let new_properties = this.state.properties
		new_properties.map((item,i)=>{
			item.cells = this.populateCells(days.getDate())
		})
		this.setState({
			date:{
				month: m,
				year: y,
				monthName: monthName[m],
				days: days.getDate()
			},
			properties: new_properties
		},()=>{
			this.getAjaxBookingByDate()
		})
	}

	populateCells(qty){
		let c = []
		for(let i=1; i<=qty; i++){
			c.push({
        id: i, 
        availability:true,
        guest: "",
        transpo: false,
        color: '',
        groupedClass: ''
      })
		}
		return c
	}

	populateProperties(qty){
		const p = this.props.property
		let p2 = []
		p.map((item,i)=>{
			p2.push({id: item.id, name: item.name, cells: this.populateCells(qty)})
		})
		return p2
	}

	componentDidMount(){
		this.getAjaxBookingByDate()
		uc.sidebar.menu_name1(this.props.property_type)
	}

	getMonthNames(){
		return ["January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"]
	}

	getAjaxBookingByDate(){
		var self = this
		var data_params = {
			year: self.state.date.year,
			month: parseInt(self.state.date.month)+1
		}
		$.ajax({
			url: `/bookings/bookingsByDate`,
			data: data_params,
			type: "POST",
			success: (s)=>{
				var newcells = self.state.properties
				s.map((record,i)=>{
					var checkin = record.checkin
					var checkout = record.checkout
					checkin = new Date(checkin)
					checkout = new Date(checkout)
					var day = checkin.getDate()-1
					var end = checkout.getDate()-1
					newcells.map((c,i)=>{
            //add color
						if(c.id == record.property_id){
							for(let i=day; i<end; i++){ 
                //insert guest name to state cells guest property
                c.cells[day]['guest'] = `${record.name} (Check-In)`
                //set condition when there is transpo, t_rate is greater than 0
                // set transpo property to true
                c.cells[day]['transpo'] = (record.t_rate>0 ? true : false)
                //set cell as false which means unavailable/booked/taken
								c.cells[i]['availability'] = false 
                //set color 
                c.cells[i]['color'] = record.source_color
                //set as group color
                if( !(end-day==1) && (i>= day && i<end-1))
                  c.cells[i]['grouped'] = "no-right-border"
              }
						}
					})
				})
				self.setState({properties: newcells})
			},
			error: (xhr, status, error)=>{
				console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
			}
		})
	}

	selectedCell(cell,p_id){
		var p = {
			day: cell,
			month: parseInt(this.state.date.month)+1,
			year: this.state.date.year,
			property_id: p_id
		}
		const dataString = `
			?day=${p.day}&
			month=${p.month}&
			year=${p.year}&
			property_id=${p.property_id}`
			window.location.href = `/reservations/new/${dataString}`
	}

	renderContainer(){
		let days = []
		for(let i=1; i<=this.state.date.days; i++){
			days.push(<th key={i}>{i}</th>)
		}
		return(
			<div className="row">
				<div className="col-md-12">
					<div className="card">
						<ReservationDate  res_updateDate={this.updateDate.bind(this)} />
						<div className="card-content table-responsive table-full-width">
						 <table className="reservation">
							 <thead className="reservationHead text-primary">
							 <tr>
									<th className="propertyHeader">Property</th>
									{days}
								</tr>
							 </thead>
							 <tbody>
               {
               this.state.properties.map((item,i)=>{
                 return(
                   <tr key={item.id}><td>{item.name}</td>
                   {item.cells.map((cell,i)=>{
                     return(
                       <ReservationCell
                       key={cell.id}
                       cell={cell}
                       property_id = {item.id}
                       selectCell={this.selectedCell} />
                     )
                   })}
                   </tr>
                   )
                 })
               }
							 </tbody>
						 </table>
						</div>
					</div>
				</div>
			</div>
		)
	}

  render () {
		return this.renderContainer()
  }
}

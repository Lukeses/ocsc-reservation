class ReservationCell extends React.Component {
	
	constructor(props){
		super(props)
		this.handleClick = this.handleClick.bind(this)
	}

	handleClick(){
		this.props.selectCell( 
			this.props.cell.id, 
			this.props.property_id
		)
	}

	availability(status){
		return (status === true ? 'available' : 'unavailable')
	}

  renderCarIcon(transpo){ 
    if(transpo){ 
      return <i className="ti-car"></i>
    }
  }

	renderCell(){
		const c = this.props.cell
		return( 
			<td 
        title= {c.guest}
        style={{backgroundColor: c.color}}
        className={`${this.availability(c.availability)} ${c.grouped}`} 
        onClick={(c.availability === true ? this.handleClick : null)}>
        {this.renderCarIcon(c.transpo)}
			</td>
		)
	}
  render () {
		return( 
			this.renderCell()
		)
  }
}


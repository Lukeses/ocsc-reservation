class ReservationDate extends React.Component {

	constructor(props){
		super(props)
		//bind
		const d = new Date()
		this.state = {
			month: d.getMonth(),
			year: d.getFullYear()
		}
		this.dateChange = this.dateChange.bind(this)
	}

	loadOldScript(){
    uc.misc.initFormExtendedDatetimepickers()
	}

	componentDidMount(){
		this.loadOldScript()
	}

	getMonths(){
    return ([
			"January",
			"February",
			"March",
			"April",
			"May",
			"June",
			"July",
			"August",
			"September",
			"October",
			"November",
			"December"
		])
	}

	getYears(){
		const d = new Date()
		//set setback for first year value
		//set forward for last year value
		const setback = 0
		const forward = 10
		const years = []
		let y = d.getFullYear()
		for(let i=(y-setback); i<=(y+forward); i++)
			years.push(i)
		return years
	}

	genListMonths(item,i){
		return(
			<option key={i}
							value={i}>
				{item}
			</option>
		)
	}

	genListYears(item,i){
		return(
			<option key={i}
							value={item}>
				{item}
			</option>
		)
	}

	dateChange(e){
		const date = e.target.value
		const name = e.target.name
		this.setState({ [name]: date },()=>{
			this.props.res_updateDate(
				this.state.month,
				this.state.year
			)
		})
	}

	renderDateSelector(){
		const title='Reservation Date'
		const instruction='Reservation calendar for room availabitlity.'
		const months = this.getMonths()
		const years = this.getYears()
		return(
			<div className="card-header">
				<div className="col-xs-12">
					<h4 className="card-title">{title}</h4>
					<hr />
					<div className="row">
						<div className="form-group col-md-2 col-sm-6 col-xs-12">
							<select className="selectpicker"
											data-style="btn btn-danger"
											onChange={this.dateChange}
											name="month"
											ref="f_month"
											value={this.state.month} >
								{months.map(this.genListMonths)}
							</select>
						</div>
						<div className="form-group col-md-3 col-sm-6 col-xs-12">
							<select className="selectpicker"
											data-style="btn btn-primary"
											onChange={this.dateChange}
											name="year"
											ref="f_year"
											value={this.state.year} >
								{years.map(this.genListYears)}
							</select>
						</div>
					</div>
					<p className="category">{instruction}</p>
				</div>
			</div>
		)
	}

  render () {
		return(
			this.renderDateSelector()
		)
  }
}

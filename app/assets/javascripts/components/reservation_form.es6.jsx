class ReservationForm extends React.Component {

  constructor(props){
    super(props)
    this.handleChangeDiscount = this.handleChangeDiscount.bind(this)
    this.renderCapacity = this.renderCapacity.bind(this)
    this.append_rental_service = this.append_rental_service.bind(this)
    this.changeProperty = this.changeProperty.bind(this)
    this.existing_client = this.existing_client.bind(this)
    this.new_client = this.new_client.bind(this)
    this.renderClientForm = this.renderClientForm.bind(this)
    this.handleChangeProperty = this.handleChangeProperty.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSave = this.handleSave.bind(this)
    this.state = {
      property: {
        id: 'none',
        name: null,
        rate: 'Select Property',
        capacity: 'Select Property',
        capacity_compare: 0
      },
      no_of_person: '',
      no_of_senior_citizen: '',
      senior_citizen: false,
      checkin: '',
      checkout: '',
      client_mode: 'new',
      client_id: 'none',
      name: '',
      company: '',
      contact: '',
      email: '',
      rental_services: [],
      discount: 'none',
      transportation: 'none',
      source: 'none',
      note: ''

    }
  }

  componentDidMount(){
    const reserve = this.props.reserve
    this.loadOldScripts()
    //when came from calendar
    //set selected property and date from calendar
    if(reserve.property_id)
      this.changeProperty(reserve.property_id)
    if(reserve.day && reserve.month && reserve.year){
      // this.setState({checkin: `${reserve.day}/${reserve.month}/${reserve.year}`})
      this.setState({checkin: `${reserve.day}/${reserve.month}/${reserve.year}`})
      this.setState({checkout: `${reserve.day}/${reserve.month}/${reserve.year}`})
    }
  }

  loadOldScripts(){
    $('.select2').select2()
    uc.misc.initFormExtendedDatetimepickers()
  }

  existing_client(){ this.setState({client_mode: "existing"}) }

  new_client(){ this.setState({client_mode: "new"}) }

  handleChangeDiscount(id){ 
    if(id=="senior_citizen")
      this.setState({senior_citizen: true})
    else
      this.setState({senior_citizen: false})
  }
  handleChangeProperty(e){
    const id = e.target.value
    this.changeProperty(id)
  }

  changeProperty(p_id){
    const id = p_id
    if(id!="none"){
      const properties = this.props.dropdowns.property
      const p = properties.filter((val)=>{
        return val.id == id
      })
      this.setState({
        property: {
          id: p[0].id,
          name: p[0].name,
          rate: p[0].rate.toFixed(2),
          capacity: `Maximum of ${p[0].capacity}`,
          capacity_compare: p[0].capacity
        }
      })
    }else{
      this.setState({
        property: {
          id: 'none',
          name: null,
          rate: 'Select Property',
          capacity: 'Select Property',
          capacity_compare: 0
        }
      })

    }
  }

  handleChange(e){
    const value = e.target.value
    const name = e.target.name
    this.setState({[name]: value},()=>{
      //special case for senior citizen discount only
      if(name=="discount")
        this.handleChangeDiscount(value)
      // console.log(this.state[name])
    })
  }

  handleSave(){
    //rental_services is not in state due to
    //select2 issues note detecting dom event
    //use jquery manually instead to get and insert value
    self = this
    this.setState({rental_services: $(`#rental_services`).val()}, ()=>{
      //start saving
      if(this.validateReservationForm(this.state)){
        // toastr.info(`Please Wait`,`Saving`)
        this.refs.btn_save.disabled = true
        $.ajax({
          url: `/reservations`,
          data: self.state,
          type: "POST",
          success: (s)=>{
            switch(s.status){
              case "success":
                toastr.success(`New Reservation`,`Saved`)
                swal({
                  title: "Saved",
                  text: "New Reservation has been saved",
                  type: "success",
                  allowEscapeKey: false,
                  allowOutsideClick: false
                }, ()=>{
                  window.setTimeout(()=> {location.href = `/reservations`}, 1)
                })
                window.setTimeout(()=> {location.href = `/reservations`}, 2000)
                break

              case "duplicate":
                toastr.warning(s.message, s.status.toUpperCase())
                this.refs.btn_save.disabled = false
                break

              case "error":
                for(let err in s.error){
                  s.error[err].map((message)=>{
                    toastr.warning(message,err.toUpperCase())
                  })
                  this.refs.btn_save.disabled = false
                }
                break
            }
          },
          error: (xhr, status, error)=>{
            toastr.error(`Something Happended`,`Error`)
            console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
            this.refs.btn_save.disabled = false
          }
        })

      }else{
        toastr.error('Please Fill Out Required Fields. Labels with *',`Incomplete Fields`)
      }
    })
  }

  validateReservationForm(f){
    let v = true

    //specific validation adding has-error to div wrapper of element
    //property
    if(f.property.id=='none'){
      $('#d_property').addClass('has-error')
      v = false
    }else{$('#d_property').removeClass('has-error')}

    //source
    if(f.source=='none'){
      $('#d_source').addClass('has-error')
      toastr.warning('Must not be Empty','Source')
      v = false
    }else{$('#d_source').removeClass('has-error')}

    //checkin
    if(f.checkin==''){
      $('#d_checkin').addClass('has-error')
      v = false
    }else{$('#d_checkin').removeClass('has-error')}

    //checkout
    if(f.checkout==''){
      $('#d_checkout').addClass('has-error')
      v = false
    }else{$('#d_checkout').removeClass('has-error')}

    // if checkin and checkout are the same
    if(f.checkout==f.checkin){
      $('#d_checkout').addClass('has-error')
      $('#d_checkin').addClass('has-error')
      toastr.warning('Check-In and Check-Out dates cannot be the same','Reservation Dates')
      v = false
    }else{
      $('#d_checkin').removeClass('has-error')
      $('#d_checkout').removeClass('has-error')
    }

    if(f.client_mode == "new"){
      //name
      if(f.name==''){
        $('#d_name').addClass('has-error')
        v = false
      }else{$('#d_name').removeClass('has-error')}
    }
    else if(f.client_mode == "existing"){
      //client (registered guets)
      if(f.client_id=='none'){
        $('#d_client').addClass('has-error')
        v = false
      }else{$('#d_client').removeClass('has-error')}
    }

    //capacity validation
    //if senior citizen is selected
    if(f.senior_citizen === false){ 
      //no_of_person
      if(f.no_of_person==''){
        $('#d_capacity').addClass('has-error')
        v = false
      }
      //if negative or 0
      else{
        if(f.no_of_person <= 0 ){
          $('#d_capacity').addClass('has-error')
          toastr.warning(`Invalid Value`, `No. of Person`)
          v = false
        }
        else if(f.no_of_person > f.property.capacity_compare ){
          $('#d_capacity').addClass('has-error')
          toastr.warning(`Exceeded Property Capacity`, `No. of Person`)
          v = false
        }
        else{ $('#d_capacity').removeClass('has-error') }
      }
    }
    else{ 
      //no_of_guest
      var no_of_guest = 0 
      no_of_guest = parseInt(f.no_of_person) + parseInt(f.no_of_senior_citizen)
      console.log(no_of_guest)
      if(f.no_of_person=='' && f.no_of_senior_citizen==''){
        $('#d_capacity').addClass('has-error')
        v = false
      }
      //if negative or 0
      else{
        if( no_of_guest <= 0){
          $('#d_capacity').addClass('has-error')
          toastr.warning(`Invalid Value`, `No. Guests`)
          v = false
        }
        else if(no_of_guest > f.property.capacity_compare ){
          $('#d_capacity').addClass('has-error')
          toastr.warning(`Exceeded Property Capacity`, `No. of Guests`)
          v = false
        }
        else{ $('#d_capacity').removeClass('has-error') }
      }
    }

    return v
  }


  append_rental_service(id, text){ 
    $('#rental_services').select2({ 
      data: [{ id: id, text: text, selected: true  }]
    })
  }
  new_rental_service(){
    $('#modal_rental_services').modal("show")
  }

  renderCapacity(){ 
    if(this.state.senior_citizen===true){ 
      return( 
        <div>
        <div className="form-group col-sm-4 col-xs-12" id="d_capacity">
          <label className="control-label">Capacity</label>
          <input className="form-control" value={this.state.property.capacity} type="text" disabled/>
        </div>
        <div className="form-group col-sm-4 col-xs-12" id="d_no_of_person">
          <label className="control-label">No. of Person(s) <star>*</star></label>
          <input className="form-control"
            onChange={this.handleChange}
            name="no_of_person"
            type="number"
            required="true"/>
        </div>
        <div className="form-group col-sm-4 col-xs-12" id="d_no_of_senior_citizen">
          <label className="control-label">No. of Senior Citizen(s) <star>*</star></label>
          <input className="form-control"
            onChange={this.handleChange}
            name="no_of_senior_citizen"
            type="number"
            required="true"/>
        </div>
        </div>
      )
    }
    else{ 
      return( 
        <div>
        <div className="form-group col-sm-8 col-xs-12" id="d_capacity">
          <label className="control-label">Capacity</label>
          <input className="form-control" value={this.state.property.capacity} type="text" disabled/>
        </div>
        <div className="form-group col-sm-4 col-xs-12" id="d_no_of_person">
          <label className="control-label">No. of Person(s) <star>*</star></label>
          <input className="form-control"
            onChange={this.handleChange}
            name="no_of_person"
            type="number"
            required="true"/>
        </div>
        </div>
      )
    }
  }
  renderForm(){
    return(
      <div>
      <QuickRentalServices append_rental_service={this.append_rental_service}/>
      <form ref="form_reservations_new" >

        <div className="card-content">

        <div className="row">

        <div className="col-sm-6">
        <div className="row">

        <div className="form-group col-sm-8 col-xs-12" id="d_property">
        <label className="control-label">Property <star>*</star></label>
        <select ref="f_property"
          className="form-control"
          type="text"
          value={this.state.property.id}
          onChange={this.handleChangeProperty}
          disabled="true"
          required="true">
            <option value="none" >Select Property</option>
            {
              this.props.dropdowns.property.map((p, i)=>{
                return <option key={p.id} value={p.id}>{p.name}</option>
              })
            }
        </select>
        </div>

        <div className="form-group col-sm-4 col-xs-12">
        <label className="control-label">Per Night Rate</label>
        <input className="form-control"
          value={this.state.property.rate}
          type="text" disabled/>
        </div>

        {this.renderCapacity()}

        <div className="form-group col-xs-6" id="d_checkin">
        <label className="control-label">Check-in Date<star>*</star></label>
        <input className="form-control datetimepicker"
          name="checkin"
          onBlur={this.handleChange}
          value={this.state.checkin}
          disabled="true"
          required="true"/>
        </div>

        <div className="form-group col-xs-6" id="d_checkout">
        <label className="control-label">Check-out Date<star>*</star>
        </label>
        <input className="form-control datetimepicker"
          name="checkout"
          onBlur={this.handleChange}
          value={this.state.checkout}
          rel="tooltip"
          title="Day/Month/Year"
          required="true"/>
        </div>


        <div className="form-group col-xs-9">
        <label className="control-label">Rentals and Services</label>
        <select ref="rental_service"
          className="form-control select2"
          id="rental_services"
          name="rental_services"
          multiple="multiple">
            {
              this.props.dropdowns.rental_service.map((s, i)=>{
                return(
                  <option key={s.id} value={s.id}>
                    {s.description} ({parseFloat(s.rate)})
                    </option>
                )
              })
            }
            </select>
        </div>

        <div className="form-group col-xs-2 pull-left">
        <label className="invisible">Add</label>
        <button className="btn btn-simple btn-magnify"
          onClick={this.new_rental_service}
          type="button"
          title="Add Rental Service">
            <i className="ti-plus"></i>
        </button>
        </div>

        </div>
        </div>

        <div className="col-sm-1"></div>

        <div className="col-sm-6">
        <div className="row">
        { this.renderClientForm() }
        <div className="form-group col-xs-12">
        <label className="control-label">Note</label>
        <textarea name="note"
          onChange={this.handleChange}
          className="form-control" />
        </div>

        </div>
        </div>
        </div>

        <hr />

        <div className="row">
        <div className="form-group col-sm-4 col-xs-12">
        <label className="control-label">Discounts</label>
        <select ref="f_discount"
          className="selectpicker"
          data-style="btn btn-success btn-block"
          title="Select Discount"
          name="discount"
          onChange={this.handleChange}
          data-size="7">
          <option value="none">No Discount</option>
          {
            this.props.dropdowns.discount.map((d, i)=>{
              return(
                <option key={d.id} value={d.id}>
                  {d.name} {d.amount} %
                  </option>
              )
            })
          }
        </select>
        </div>

        <div className="form-group col-sm-4 col-xs-12" id="d_transportation">
        <label className="control-label">Transportation </label>
        <select ref="f_transportation"
          className="selectpicker"
          data-style="btn btn-danger btn-block"
          title="Select Transpo Service"
          name="transportation"
          onChange={this.handleChange}
          data-size="7">
          <option value="none">No Transpo Service (0.00)</option>
          {
            this.props.dropdowns.transportation.map((t, i)=>{
              return(
                <option key={t.id} value={t.id}>
                  {t.description} ({t.rate})
                  </option>
              )
            })
          }
        </select>
        </div>

        <div className="form-group col-sm-4 col-xs-12" id="d_source">
        <label className="control-label">Source <star>*</star></label>
        <select ref="f_source"
          className="selectpicker"
          data-style="btn btn-warning btn-block"
          title="Select Source"
          name="source"
          onChange={this.handleChange}
          data-size="7">
          <option value="none">Select Source</option>
          {
            this.props.dropdowns.sources.map((s, i)=>{
              return(
                <option key={s.id} value={s.id}>
                  {s.name}
                  </option>
              )
            })
          }
        </select>
        </div>
        </div>

        <div className="category">
        <star>*</star>
        Required fields
        </div>
        </div>
        <div className="card-footer text-center">
        <button  type="button"
          onClick={this.handleSave}
          ref="btn_save"
          className="btn_save btn btn-lg btn-info btn-fill btn-wd" >
          Save Reservation
        </button>
        </div>
        </form>
        </div>
    )
  }

  renderClientForm(){
    if(this.state.client_mode == "new")
      return this.renderNewClient()
    else
      return this.renderExistingClient()
  }

  renderExistingClient(){
    return(
      <div>
        <div className="form-group col-xs-1">
        <label className="invisible">Search</label>
        <button className="btn btn-icon btn-magnify"
          onClick={this.new_client}
          type="button"
          title="Add New Guest">
            <i className="ti-plus"></i>
        </button>
        </div>

        <div className="form-group col-sm-11 col-xs-12" id="d_client">
        <label className="control-label">Registered Guest<star>*</star></label>
        <select className="form-control"
          name="client_id"
          onChange={this.handleChange}
          required="true">
          <option value="none">Select Guest</option>
          {
            this.props.dropdowns.clients.map((client)=>{
              return(
                <option value={client.id} key={client.id} >{client.name}</option>
              )
            })
          }
        </select>
        </div>
        </div>
    )
  }

  renderNewClient(){
    return(
      <div>
        <div className="form-group col-xs-1">
        <label className="invisible">Search</label>
        <button className="btn btn-icon btn-magnify"
      onClick={this.existing_client}
      type="button"
      title="Search Registered Guest">
        <i className="ti-search"></i>
        </button>
        </div>

        <div className="form-group col-sm-5 col-xs-12" id="d_name">
        <label className="control-label">Guests Full Name<star>*</star></label>
        <input className="form-control"
      onChange={this.handleChange}
      name="name"
      type="text"
      required="true"/>
        </div>

        <div className="form-group col-sm-6 col-xs-12">
        <label className="control-label">Company Name</label>
        <input className="form-control" onChange={this.handleChange} name="company" type="text" />
        </div>

        <div className="form-group col-sm-6 col-xs-12">
        <label className="control-label">Email Address</label>
        <input className="form-control" onChange={this.handleChange} name="email" type="email" />
        </div>

        <div className="form-group col-sm-6 col-xs-12">
        <label className="control-label">Contact Number</label>
        <input className="form-control" name="contact" onChange={this.handleChange} type="number" />
        </div>
        </div>

    )
  }

  render () {
    const dropdowns = this.props.dropdowns
    return(
      <div>
        {this.renderForm()}
        </div>
    )
  }
}

class ReservationRecords extends React.Component {

  constructor(props){
    super(props)
    this.handleSearch = this.handleSearch.bind(this)
    this.handleUnfilter = this.handleUnfilter.bind(this)
    this.handleFilter = this.handleFilter.bind(this)
    this.filter = this.filter.bind(this)
    this.getAjaxRecords = this.getAjaxRecords.bind(this)
    this.recordCheckIn = this.recordCheckIn.bind(this)
    this.recordCheckOut = this.recordCheckOut.bind(this)
    this.receipt = this.receipt.bind(this)
    this.renderReceipt = this.renderReceipt.bind(this)
    this.renderMain = this.renderMain.bind(this)
    this.handlePayment = this.handlePayment.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
    this.modal = {
      id: "modal_view",
      size: "modal-md",
      icon: "",
      title: "Reservation Information",
      filter: { 
        id: "modal_filter",
        size: "modal-xs",
        icon: "",
        title: "Filter"
      }
    }
    this.state = {
      records:  [],
      receipt: {}
    }
  }

  loadOldScripts(){
    //$(`#dt_reservation_records`).dataTable()
    uc.sidebar.menu_name1(`reservations`)
  }

  componentDidMount(){
    this.loadOldScripts()
    this.getAjaxRecords()
  }

  recordCheckIn(soli){
    let palit = this.state.records
    palit[soli.index].status2 = 2
    this.setState({
      records: palit
    },()=>{this.updateReservation(palit[soli.index].id, 'checkin')})
  }

  recordCheckOut(soli){
    let palit = this.state.records
    palit[soli.index].status2 = 3
    this.setState({
      records: palit
    },()=>{this.updateReservation(palit[soli.index].id, 'checkout')})
  }

  updateReservation(id,type){
    self = this
    $.ajax({
      url: `/reservations/${type}`,
      data: {id: id},
      type: "POST",
      success: (s)=>{ self.getAjaxRecords() },
      error: (xhr, status, error)=>{
        console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
      }
    })
  }

  getAjaxRecords(){
    self = this
    $.ajax({
      url: `/reservations/records`,
      type: "POST",
      success: (s)=>{
        self.setState({records: s})
      },
      error: (xhr, status, error)=>{
        console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
      }
    })
  }


	handleSearch(s){
		this.setState({records: s})
	}
  handleCancel(){
    self = this
    //confirmation
    swal({
      title: 'Are you sure?',
      text: "Cancel this Reservation",
      type: 'warning',
      showCancelButton: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm Cancellation'
    }).then(function () {
      $.ajax({
        url: `/reservations/cancel`,
        data: {id: self.state.receipt.id},
        type: "POST",
        success: (s)=>{
          switch(s.status){
            case "success":
              self.setState({receipt: {}},()=>{
                $(`#${self.modal.id}`).modal('hide')
                self.getAjaxRecords()
                toastr.info(`Reservation
                  has been Cancelled`,
                  `Cancellation  Done`)
              })
              break
          }
        },
        error: (xhr, status, error)=>{
          console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
        }
      })
    })
  }

  clearPayment(){
    this.refs.amount.value = ""
  }

  handlePayment(){
    self = this
    const r = this.state.receipt
    const total =
      parseFloat((r.total-r.d_rate)-r.total_payments)
    const amount =
      parseFloat(this.refs.amount.value)
    if(amount){
      if(amount<=total){
        this.refs.btn_payment.disabled = true
          //confirmation
          swal({
            title: 'Make Payment?',
            text: "Pay with this amount?",
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
          }).then(function () {
            $.ajax({
              url: `/payments`,
              type: "POST",
              data: {amount: amount, booking_id: r.id},
              success: (s)=>{
                switch (s.status) {
                  case 'success':
                    self.refs.btn_payment.disabled = false
                    self.clearPayment()
                    self.getAjaxRecords()
                    $(`#${self.modal.id}`).modal('hide')
                    swal(
                      'Payment Made','',
                      'success'
                    )
                    break
                  case 'error':
                    for(let err in s.error){
                      s.error[err].map((message)=>{
                        toastr.warning(message,err.toUpperCase())
                      })
                      self.refs.btn_payment.disabled = false
                    }
                    break
                }
              },
              error: (xhr, status, error)=>{
                console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
                self.refs.btn_payment.disabled = false
              }



          })
        })
        this.refs.btn_payment.disabled = false
      }else{
        swal("Exceeded Amount Due","","warning")
      }
    }else{ toastr.info('Enter an Amount','Payment Field is Blank') }
  }

  receipt(record){
    //clears first
    this.setState({receipt: {}},()=>{
      //pre load receipt state
      this.setState({receipt: record},()=>{
        this.clearPayment()
        $(`#${this.modal.id}`).modal('show')
      })
    })
  }

  renderReceipt(){
    const r = this.state.receipt
    const amount_due = (r.total-r.d_rate)-r.payment
    const amount_due_class = (amount_due == 0 ? "hidden" : "")
    const t = uc.transform
    return(
      <div>
        <table className="table">
        <tbody>
        <tr>
        <th>Property Name</th>
        <td>{r.property_name}</td>
        </tr>
        <tr>
        <th>Guest</th>
        <td>{r.name}</td>
        </tr>

        <tr>
        <th>Check-In</th>
        <td>{t.format_d_m_y(null,r.checkin)}</td>
        </tr>

        <tr>
        <th>Check-Out</th>
        <td>{t.format_d_m_y(null,r.checkout)}</td>
        </tr>

        <tr>
        <th>Rate:</th>
        <td>{t.peso(null, (r.p_rate/r.nights) )}</td>
        </tr>

        <tr>
        <th>No Of Nights: </th>
        <td>{r.nights}</td>
        </tr>

        <tr>
        <th>Subtotal:</th>
        <td>{t.peso(null,r.p_rate)}</td>
        </tr>

        <tr>
        <th>Transporation Charge:</th>
        <td>{t.peso(null,r.t_rate)}</td>
        </tr>

        <tr>
        <th>Rentals and Services:</th>
        <td>{t.peso(null,r.rs_rate)}</td>
        </tr>

        <tr className="info">
        <th>Discounts and Promos</th>
        <td>{`(LESS ${t.peso(null,r.d_rate)} )`}</td>
        </tr>

        <tr className="warning">
        <th>Total</th>
        <td>{t.peso(null,r.total)}</td>
        </tr>

        <tr className="success">
        <th>Paid Amount</th>
        <td>{t.peso(null,r.payment)}</td>
        </tr>

        <tr className="danger">
        <th>Total Amount Due</th>
        <td>{t.peso(null,amount_due)}</td>
        </tr>

        </tbody>
        </table>
        <div className={`input-group ${amount_due_class}`}>
          <input className="form-control"
            ref="amount"
            placeholder="Enter Payment Amount"
            type="number"/>
          <span className="input-group-btn">
            <button
              ref="btn_payment"
              onClick={this.handlePayment}
              className={`btn btn-success btn-fill btn-magnify ${amount_due_class}`}>
              <i className="ti-plus"></i> Make Payment
            </button>
          </span>
        </div>
        <div className="card-footer text-center">
        <div className="btn-group">
          <a
            href={`/reservations/voucher?booking_id=${r.id}`}
            target="_new"
            type="button"
            ref="btn_voucher"
            onClick={this.handleVoucher}
            className={`btn btn-primary btn-fill btn-wd`}>
            <i className="ti-printer"></i> Print
          </a>
          <a
            href={`/reservations/voucher?booking_id=${r.id}`}
            target="_new"
            type="button"
            ref="btn_mailer"
            onClick={this.handleMailer}
            className={`btn btn-primary btn-fill btn-wd`}>
            <i className="ti-email"></i> Mail
          </a>
          <button
            type="button"
            onClick={this.handleCancel}
            className="btn btn-danger btn-fill btn-wd">
            <i className="ti-close"></i> Cancel Booking
          </button>
        </div>
        </div>
        </div>
    )
  }


  handleUnfilter(){ 
    this.getAjaxRecords()
    this.refs.filter_checkin.value = ""
    $(`#${this.modal.filter.id}`).modal('hide')
  }
  handleFilter(){ 
    self = this
    const data = { 
      checkin: this.refs.filter_checkin.value
    }

    $.ajax({
      url: `/reservations/filter`,
      data: data,
      type: "POST",
      success: (s)=>{
        switch(s.status){
          case "success":
            self.setState({records: s.records},()=>{ 
              $(`#${self.modal.filter.id}`).modal('hide')
            })
          break
        }
      },
      error: (xhr, status, error)=>{
        toastr.error(`Something Happended`,`Error`)
        console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
      }
    })
  }
  renderFilter(){ 
    return( 
      <div>
        <div className="row text-center">
          <div className="col-sm-8 col-sm-offset-2">
            <label 
              className="control-label text-left">
              Check-In Date
            </label>
            <input
              ref="filter_checkin"
              className="form-control"
              type="date"
              name="filter_checkin"/>
            <hr/>
            <div className="btn-group">
              <button 
                onClick={this.handleUnfilter}
                className="btn btn-warning btn-fill btn-wd">
                <i className="ti-close"></i> Remove Filter
              </button>
              <button 
                onClick={this.handleFilter}
                className="btn btn-info btn-fill btn-wd">
                <i className="ti-filter"></i> Filter
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
  filter(){ 
    $(`#${this.modal.filter.id}`).modal('show')
  }
  renderMain(){
    const thead=[
      'Guest',
      'Transpo',
      'CheckIn',
      'CheckOut',
      'Property',
      'Source',
      'Balance',
      'Payment',
      'Reservation',
      'Status'
    ]
    const tdata=[
      {column: 'name'},
      {column: 'transportation_name'},
      {column: 'checkin', transform: 'format_d_m_y'},
      {column: 'checkout', transform: 'format_d_m_y'},
      {column: 'property_name'},
      {column: 'source_name'},
      {column: 'balance', transform: 'peso'},
      {column: 'payment', transform: 'peso'},
      {column: 'status2', transform: 'status_reservation'},
      {column: 'status3', transform: 'status_payment'}
    ]
    return(
      <div>
        <Modal modal={this.modal}>
        {this.renderReceipt()}
        </Modal>
        <Modal modal={this.modal.filter}>
        {this.renderFilter()}
        </Modal>
        <Container>
        <TableSearch 
          searched={this.handleSearch}
          url="reservations/search">
          <Filter filter={this.filter}/>
        </TableSearch>
        <Table
          data={this.state.records}
          thead = {thead}
          tdata={tdata}
          taction="reservation_records"
          checkin = {this.recordCheckIn}
          checkout = {this.recordCheckOut}
          receipt={this.receipt}
          classes = "table-hover table-striped"
        />
        </Container>
        </div>
    )
  }

  render () {
    return(
      <div>
        {this.renderMain()}
        </div>
    )
  }

}

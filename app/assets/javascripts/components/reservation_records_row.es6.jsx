class ReservationRecordsRow extends React.Component {

  constructor(props){
    super(props)
    this.Checkin = this.Checkin.bind(this)
    this.Checkout = this.Checkout.bind(this)
    this.View = this.View.bind(this)
  }

  componentDidMount(){
  }

  setCellColor(s,type){
    if(type=="reservation"){
      if(s==1){return "warning"}
      else if(s==2){return "info"}
      else if(s==3){return "success"}
    }
    else if(type=="payment"){
      if(s==1){return "danger"}
      else if(s==2){return "success"}
    }
  }

  setStatusName(s,type){
    if(type=="reservation"){
      if(s==1){return "Reserved"}
      else if(s==2){return "Checked-In"}
      else if(s==3){return "Checked-Out"}
    }
    else if(type=="payment"){
      if(s==1){return "Amount Due"}
      else if(s==2){return "Fully Paid"}
    }
  }

  setDisability(s,type){
    if(type=="checkin"){
      if(s==1){return false}
      else{return true }
    }
    if(type=="checkout"){
      if(s==2){return false}
      else{return true }
    }
  }

  formatDate(date){
    return `${date.getDate()}/${date.getMonth()}/${date.getFullYear()} `
  }

  Checkin(){
    self = this
    swal({
      title: 'Confirm Check In',
      text: "",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Check In'
    }).then(function () {
      self.props.checkin({id: self.props.record.id, index: self.props.index})
      swal(
        'Checked In!',
        'Reservation has been Checked In',
        'success'
      )
      toastr.success('Checked In')
    })
  }

  Checkout(){
    self = this
    swal({
      title: 'Confirm Check Out',
      text: "",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Check Out'
    }).then(function () {
      self.props.checkout({id: self.props.record.id, index: self.props.index})
      swal(
        'Checked Out!',
        'Successfully set Checked Out',
        'success'
      )
      toastr.success('Checked Out')
    })
  }

  View(){
    self = this
    var pass = this.props.record
    $.ajax({
      url: `/payments/total_payment`,
      type: "POST",
      data: {booking_id: this.props.record.id},
      success: (s)=>{
        pass.total_payments = s
        self.props.receipt(pass)
      },
      error: (xhr, status, error)=>{
        console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
      }
    })
  }

  renderRow(){
    const r = this.props.record
    return(
      <div className="table-icons">
        <button rel="tooltip"
      className="btn btn-simple btn-info btn-icon table-action view"
      ref="action_checkin"
      disabled={this.setDisability(r.status2, "checkin")}
      onClick={this.Checkin}
      data-original-title="Check-in" >
        <i className="ti-tag"></i>
        </button>
        <button rel="tooltip"
      className="btn btn-simple btn-warning btn-icon table-action edit"
      disabled={this.setDisability(r.status2, "checkout")}
      onClick={this.Checkout}
      data-original-title="Check-out">
        <i className="ti-bell"></i>
        </button>
        <button rel="tooltip"
      className="btn btn-simple btn-danger btn-icon table-action remove"
      onClick={this.View}
      data-original-title="View Receipt">
        <i className="ti-stamp"></i>
        </button>
        </div>
    )
  }

  render () {
    return(this.renderRow())
  }
}

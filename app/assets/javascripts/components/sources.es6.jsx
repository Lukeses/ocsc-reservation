class Sources extends React.Component {

	constructor(props){
		super(props)
    this.renderMain = this.renderMain.bind(this)
		this.handleSearch = this.handleSearch.bind(this)
    this.getAjaxRecords = this.getAjaxRecords.bind(this)
		this.new = this.new.bind(this)
		this.delete = this.delete.bind(this)
		this.fetch = this.fetch.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.save = this.save.bind(this)
		this.update = this.update.bind(this)
    this.modal = {
      id: "modal_sources",
      size: "modal-md",
      icon: "",
      title: "Source"
    }
		this.state = {
			records:  [],
			mode: null,
			name: '',
      color: ''
		}
	}

	loadOldScripts(){
		uc.sidebar.menu_name1(`sources`)
	}

	componentDidMount(){
		this.loadOldScripts()
		this.getAjaxRecords()
	}

	getAjaxRecords(){
		self = this
		$.ajax({
			url: `/sources/records`,
			type: "POST",
			success: (s)=>{
				self.setState({records: s})
			},
			error: (xhr, status, error)=>{
				console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
			}
		})
	}

	handleSearch(s){
		this.setState({records: s})
	}

	handleChange(e){
		const value = e.target.value
		const name = e.target.name
		this.setState({[name]: value},()=>{
			// console.log(this.state[name])
		})
	}

	showModal(){
		$(`#${this.modal.id}`).modal('show')
	}

	validate(f){
		let v = true
		//specific validation adding has-error to div wrapper of element
		//name
		if(f.name==''){
			$('#d_name').addClass('has-error')
			v = false
		}else{$('#d_name').removeClass('has-error')}

		if(f.color==''){
			$('#d_color').addClass('has-error')
			v = false
		}else{$('#d_color').removeClass('has-error')}
    //return validation result
		return v
	}

  save(callback){
		self = this
		const data = {
			name: this.state.name,
      color: this.state.color
		}
		//validate
		if(this.validate(this.state)){
			$.ajax({
				url: `/sources`,
				data: data,
				type: "POST",
				success: (s)=>{
					switch(s.status){
						case "success":
							self.clear()
							self.getAjaxRecords()
							toastr.success(`New Source`,`Saved`)
							swal({
								title: "Saved",
								text: "New Source has been saved",
								type: "success",
								allowEscapeKey: false,
								allowOutsideClick: false
							}, ()=>{
									// do something after clicking ok of swal
							})
						break
						case "duplicate":
							toastr.warning(s.message, s.status.toUpperCase())
							// this.refs.btn_save.disabled = false
						break
					}
				},
				error: (xhr, status, error)=>{
					toastr.error(`Something Happended`,`Error`)
					console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
					// this.refs.btn_save.disabled = false
				}
			})

		}else{
			toastr.error('Please Fill Out Required Fields. Labels with *',`Incomplete Fields`)
		}
		callback()
  }

  update(id,callback){
    self = this
		const data = {
			name: this.state.name,
      color: this.state.color
		}
		if(this.validate(this.state)){
			$.ajax({
				url: `/sources/${id}`,
				data: data,
				type: "patch",
				success: (s)=>{
					switch(s.status){
						case "success":
							self.setState({mode: "save"})
							self.clear()
							self.getAjaxRecords()
							toastr.success(`New Source`,`Updated`)
							swal({
								title: "Updated",
								text: "Source has been Updated",
								type: "success",
								allowescapekey: false,
								allowoutsideclick: false
							}, ()=>{
									// do something after clicking ok of swal
							})
						break
						case "duplicate":
							toastr.warning(s.message, s.status.toUpperCase())
							// this.refs.btn_save.disabled = false
						break
					}
				},
				error: (xhr, status, error)=>{
					toastr.error(`something happended`,`error`)
					console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
					// this.refs.btn_save.disabled = false
				}
			})

		}else{
			toastr.error('please fill out required fields. labels with *',`incomplete fields`)
		}
		callback()
  }

	new(){
		this.clear()
		this.setState({mode: "save"},()=>{
			this.showModal()
		})
	}

  fetch(id, index){
		self = this
		this.clear()
		this.setState({mode: id},()=>{
      $('input[name="color"]').minicolors('value',{color: this.state.records[index].color})
      $('input[name="color"]').val(this.state.records[index].color)
			self.setState({
				name: this.state.records[index].name,
        color: this.state.records[index].color
			},()=>{
				this.showModal()
			})
		})
  }

	clear(){
		this.setState({name: '', color: ''},()=>{
			$('#d_name').removeClass('has-error')
			$('#d_color').removeClass('has-error')
      $('input[name="color"]').minicolors('value',{color: null})
      $('input[name="color"]').val('')
		})
	}

  delete(id, index){
    self = this
    let newRecords = this.state.records
    //confirmation
    swal({
      title: 'Are you sure?',
      text: "You are delete this record!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Delete'
    }).then(function () {
			$.ajax({
				url: `/sources/${id}`,
				type: "DELETE",
				success: (s)=>{
					switch(s.status){
						case "success":
				      newRecords.splice(index,1)
				      self.setState({records: newRecords},()=>{
				        toastr.info(`Record successfully removed`,`Deleted`)
				      })
						break
					}
				},
				error: (xhr, status, error)=>{
					console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
				}
			})
    })
  }

  renderMain(){
		var fields = {
			name: this.state.name
		}
    return(
      <div>
				<SourcesForm fields={fields}
					change={this.handleChange}
					save={this.save}
					update={this.update}
					modal={this.modal}
					mode={this.state.mode}/>
				<AddNewButton add_new={this.new}/>
        <Container>
					<TableSearch searched={this.handleSearch}
						url="sources/search"/>
          <Table data={this.state.records}
            thead={['Source Name', 'Color']}
            tdata={[{column: 'name'}, {column: 'color'}]}
						deleteAction={this.delete}
						editAction={this.fetch}
            classes="table-hover table-striped" />
        </Container>
      </div>
    )
  }

  render () {
    return this.renderMain()
  }
}

class SourcesForm extends React.Component {
  constructor(props){
    super(props)
    this.renderMain = this.renderMain.bind(this)
  }

  componentDidMount(){
   $("input[name='color']").minicolors({  theme: 'bootstrap' })
  }

  renderMain(){
    return(
      <Modal modal={this.props.modal}>
        <form  id="form_sources">
          <div className="row">
            <div className="col-sm-12">
							<div className="form-group col-sm-7"
                id="d_name">
								<label className="control-label">Name<star>*</star></label>
								<input 
                  className="form-control"
                  onChange={this.props.change}
                  value={this.props.fields.name}
                  name="name"
                  title="Input Full Name"
                  required="true"/>
							</div>
							<div className="form-group col-sm-5"
                id="d_color">
								<label className="control-label">Color</label>
								<input 
                  onBlur={this.props.change}
                  value={this.props.fields.color}
                  className="form-control"
                  name="color" />
							</div>
            </div>
          </div>
          <ModalSaveUpdate mode={this.props.mode}
            update={this.props.update}
            save={this.props.save}/>
        </form>
      </Modal>
    )
  }

  render () {
    return this.renderMain()
  }
}

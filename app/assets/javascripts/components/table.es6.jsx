class Table extends React.Component {
  constructor(props){
    super(props)
    this.renderMain = this.renderMain.bind(this)
    this.renderTableData = this.renderTableData.bind(this)
    this.tableAction = this.tableAction.bind(this)
  }
  tableAction(row,i){
    switch (this.props.taction) {
      case 'reservation_records':
        return(
          <ReservationRecordsRow key={i}
          record={row}
          checkin = {this.props.checkin}
          checkout = {this.props.checkout}
          receipt = {this.props.receipt}
          index={i}/>
        )
        break;
      default:
        return(
          <Actions index={i} id={row.id}
          editAction={this.props.editAction}
          deleteAction={this.props.deleteAction}/>
        )
    }
  }
  renderTableData(row,i){
    return(
      <tr key={i} >
        <td>{(i+1)}</td>
        {
          this.props.tdata.map((data,i)=>{
            return(
              <td key={i}>
                {this.processData(row,data) }
                </td>
            )
          })
        }
        <td>{this.tableAction(row,i)}</td>
        </tr>
    )
  }

  processData(row,data){
    if(row[data.column]){
      if(data.transform){
        return uc.transform[data.transform](row,row[data.column])
      }else{
        return row[data.column]
      }
    }
  }

  emptyTable(){
    const length = $("table > thead > tr:first > th").length
    return(
      <tr>
        <td colSpan={length}>
          <div className="text-center">
            <h5>No Record</h5>
            <Img 
              className="no-record"
              src="/assets/search2.gif" />
          </div>
        </td>
      </tr>
    )
  }

  renderMain(){
    const data = this.props.data
    return(
      <table id={`#table`} className={`table sortable ${this.props.classes}`}>
        <thead>
          <tr>
            <th>#</th>
              {this.props.thead.map((value,i)=>{
                return <th key={i}>{value}</th>
              })}
            <th data-defaultsort="disabled">Actions</th>
          </tr>
        </thead>
        <tbody>
          { (data.length == 0 ? this.emptyTable() : this.props.data.map(this.renderTableData)) }
        </tbody>
      </table>
    )
  }
  render () {
    return this.renderMain()
  }
}

class TableSearch extends React.Component {
  constructor(props){
    super(props)
    this.renderMain = this.renderMain.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(e){
		self = this
		$.ajax({
			url: this.props.url,
      data: {search: e.target.value},
			type: "POST",
			success: (s)=>{
        self.props.searched(s)
			},
			error: (xhr, status, error)=>{
				console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
			}
		})
  }

  renderMain(){
    return(
			<div className="row">
				<div className="col-sm-3 col-xs-12 form-group">
					<input className="form-control"
            onChange={this.handleChange}
            placeholder="Search"/>
				</div>
        {this.props.children}
			</div>
    )
  }
  render () {return this.renderMain()}
}


/*
  Use Component before table
  In parent component , create a function that will replace
  the current state with the returned records of this Component like this:

		<TableSearch searched={this.handleSearch}
			url="clients/search"/>

  In Prent component, add props like this:

  	<TableSearch searched={this.handleSearch}/>
*/

class Transportations extends React.Component {

	constructor(props){
		super(props)
    this.renderMain = this.renderMain.bind(this)
		this.handleSearch = this.handleSearch.bind(this)
    this.getAjaxRecords = this.getAjaxRecords.bind(this)
		this.new = this.new.bind(this)
		this.delete = this.delete.bind(this)
		this.fetch = this.fetch.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.save = this.save.bind(this)
		this.update = this.update.bind(this)
		this.getFields = this.getFields.bind(this)
    this.modal = {
      id: "modal_transportations",
      size: "modal-md",
      icon: "",
      title: "Transportations"
    }
		this.state = {
			records:  [],
			mode: null,
			description: '',
			rate: ''
		}
	}

	loadOldScripts(){
		uc.sidebar.menu_name1(`transportations`)
	}

	componentDidMount(){
		this.loadOldScripts()
		this.getAjaxRecords()
	}

	getAjaxRecords(){
		self = this
		$.ajax({
			url: `/transportations/records`,
			type: "POST",
			success: (s)=>{
				self.setState({records: s})
			},
			error: (xhr, status, error)=>{
				console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
			}
		})
	}

	getFields(){
		return({
			description: this.state.description,
			rate: this.state.rate
		})
	}

	handleSearch(s){
		this.setState({records: s})
	}

	handleChange(e){
		const value = e.target.value
		const name = e.target.name
		this.setState({[name]: value},()=>{
			// console.log(this.state[name])
		})
	}

	showModal(){
		$(`#${this.modal.id}`).modal('show')
	}

	validate(f){
		let v = true
		//specific validation adding has-error to div wrapper of element
		//name
		if(f.description==''){
			$('#d_description').addClass('has-error')
			v = false
		}else{$('#d_description').removeClass('has-error')}
		//amount
		if(f.rate=='' || f.rate<=0 || f.rate>=90000){
			$('#d_rate').addClass('has-error')
			v = false
		}else{$('#d_rate').removeClass('has-error')}
    //return validation result
		return v
	}

  save(callback){
		self = this
		const data = this.getFields()
		//validate
		if(this.validate(this.state)){
			$.ajax({
				url: `/transportations`,
				data: data,
				type: "POST",
				success: (s)=>{
					switch(s.status){
						case "success":
							self.clear()
							self.getAjaxRecords()
							toastr.success(`New Discount`,`Saved`)
							swal({
								title: "Saved",
								text: "New Transportation has been saved",
								type: "success",
								allowEscapeKey: false,
								allowOutsideClick: false
							}, ()=>{
									// do something after clicking ok of swal
							})
						break
						case "duplicate":
							toastr.warning(s.message, s.status.toUpperCase())
							// this.refs.btn_save.disabled = false
						break
					}
				},
				error: (xhr, status, error)=>{
					toastr.error(`Something Happended`,`Error`)
					console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
					// this.refs.btn_save.disabled = false
				}
			})

		}else{
			toastr.error('Please Fill Out Required Fields. Labels with *',`Incomplete Fields`)
		}
		callback()
  }

  update(id,callback){
    self = this
		const data = this.getFields()
		if(this.validate(this.state)){
			$.ajax({
				url: `/transportations/${id}`,
				data: data,
				type: "patch",
				success: (s)=>{
					switch(s.status){
						case "success":
							self.setState({mode: "save"})
							self.clear()
							self.getAjaxRecords()
							toastr.success(`New Discount`,`Updated`)
							swal({
								title: "Updated",
								text: "Discount has been Updated",
								type: "success",
								allowescapekey: false,
								allowoutsideclick: false
							}, ()=>{
									// do something after clicking ok of swal
							})
						break
						case "duplicate":
							toastr.warning(s.message, s.status.toUpperCase())
							// this.refs.btn_save.disabled = false
						break
					}
				},
				error: (xhr, status, error)=>{
					toastr.error(`something happended`,`error`)
					console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
					// this.refs.btn_save.disabled = false
				}
			})

		}else{
			toastr.error('please fill out required fields. labels with *',`incomplete fields`)
		}
		callback()
  }

	new(){
		this.clear()
		this.setState({mode: "save"},()=>{
			this.showModal()
		})
	}

  fetch(id, index){
		self = this
		this.clear()
		this.setState({mode: id},()=>{
			self.setState({
				description: this.state.records[index].description,
				rate: this.state.records[index].rate
			},()=>{
				this.showModal()
			})
		})
  }

	clear(){
		this.setState({
			description: '',
			rate: ''
		},()=>{
			$('#d_description').removeClass('has-error')
			$('#d_rate').removeClass('has-error')
		})
	}

  delete(id, index){
    self = this
    let newRecords = this.state.records
    //confirmation
    swal({
      title: 'Are you sure?',
      text: "You are delete this record!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Delete'
    }).then(function () {
			$.ajax({
				url: `/transportations/${id}`,
				type: "DELETE",
				success: (s)=>{
					switch(s.status){
						case "success":
				      newRecords.splice(index,1)
				      self.setState({records: newRecords},()=>{
				        toastr.info(`Record successfully removed`,`Deleted`)
				      })
						break
					}
				},
				error: (xhr, status, error)=>{
					console.log(`xhr: ${xhr.status}, status: ${status}, error: ${error}`)
				}
			})
    })
  }

  renderMain(){
		const fields = this.getFields()
    return(
      <div>
				<TransportationsForm fields={fields}
					change={this.handleChange}
					save={this.save}
					update={this.update}
					modal={this.modal}
					mode={this.state.mode}/>
				<AddNewButton add_new={this.new}/>
        <Container>
					<TableSearch searched={this.handleSearch}
						url="transportations/search"/>
          <Table data={this.state.records}
            thead={['Description','Rate']}
            tdata={[
							{column: 'description'},
							{column: 'rate', transform: 'peso'}
						]}
						deleteAction={this.delete}
						editAction={this.fetch}
            classes="table-hover table-striped" />
        </Container>
      </div>
    )
  }

  render () {
    return this.renderMain()
  }
}

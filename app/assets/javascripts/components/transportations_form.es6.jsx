class TransportationsForm extends React.Component {
  constructor(props){
    super(props)
    this.renderMain = this.renderMain.bind(this)
  }

  renderMain(){
    return(
      <Modal modal={this.props.modal}>
        <form  id="form_transportations">
          <div className="row">
            <div className="col-sm-12">
							<div className="form-group col-xs-8"
                id="d_description">
								<label className="control-label">Description<star>*</star></label>
								<input className="form-control"
											 onChange={this.props.change}
                       value={this.props.fields.description}
											 name="description"
                       title="Input Transportatoin Name / Description"
											 required="true"/>
							</div>
							<div className="form-group col-xs-4"
                id="d_rate">
								<label className="control-label">Rate<star>*</star></label>
								<input className="form-control"
											 onChange={this.props.change}
                       value={this.props.fields.rate}
                       type="number"
											 name="rate"
											 required="true"/>
							</div>
            </div>
          </div>
          <ModalSaveUpdate mode={this.props.mode}
            update={this.props.update}
            save={this.props.save}/>
        </form>
      </Modal>
    )
  }

  render () {
    return this.renderMain()
  }
}

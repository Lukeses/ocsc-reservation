class BookingsController < ApplicationController

  before_action :naka_login_ba?

  def index
		@sources = Source.records
		@transportation = Transportation.records
		@discount = Discount.records
		@property = Property.rooms
		@rental_service = RentalService.records
		@bookings = Booking.records
  end

  def cottages
		@sources = Source.records
		@transportation = Transportation.records
		@discount = Discount.records
		@property = Property.cottages
		@rental_service = RentalService.records
		@bookings = Booking.records
  end

  def events
		@sources = Source.records
		@transportation = Transportation.records
		@discount = Discount.records
		@property = Property.events
		@rental_service = RentalService.records
		@bookings = Booking.records
  end

  def create
  end

  def update
  end

  def destroy
  end

  def show
  end

	def bookingsByDate
		record = Booking.recordsByDate(params[:year],params[:month])
		render json: record.as_json
	end

	private
end

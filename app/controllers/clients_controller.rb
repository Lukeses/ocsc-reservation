class ClientsController < ApplicationController

  before_action :naka_login_ba?

  def index
  end
  def create
		#if has the same name
		if Client.where(name: params[:name].titleize.strip).count == 0
			client_id = generate_id('CL',Client)
			client = Client.new do |c|
				c.id = client_id
				c.name = params[:name].titleize.strip
				c.company = params[:company].titleize.strip
				c.email = params[:email].strip
				c.contact = params[:contact].strip
			end
      if client.save
        render json: {status: "success"}.as_json
      end
		else
			render(json: {status: "duplicate",
				message: "Duplicate Guest Name. There's a
				registered Guest with the same name"}.as_json)
		end
  end
  def update
		#if has the same name
		if Client.where(name: params[:name].
        titleize.strip).where.not(id: params[:id]).count == 0
			client = Client.find_by id: params[:id]
			client.name = params[:name].titleize.strip
			client.company = params[:company].titleize.strip
			client.email = params[:email].strip
			client.contact = params[:contact].strip
      if client.save
        render json: {status: "success"}.as_json
      end
		else
			render(json: {status: "duplicate",
				message: "Duplicate Guest Name. There's a
				registered Guest with the same name"}.as_json)
		end
  end
  def destroy
		client = Client.find_by id: params[:id]
		client.status = 0
		if client.save
			render json: {status: "success"}.as_json
		else
			render json: {status: "error"}.as_json
		end
  end

  def records
    render json: Client.records.as_json
  end

  def search
    s = params[:search]
    records = Client.where("status = 1 AND
      ((name LIKE ?) OR (company LIKE ?) OR
      (contact LIKE ?) OR (email LIKE ?))",
        "%#{s}%","%#{s}%","%#{s}%","%#{s}%")
    render json: records.as_json
  end
end

class DiscountsController < ApplicationController

  before_action :naka_login_ba?
  def index
  end

  def create
		#if has the same name
		if Discount.where(name: params[:name].titleize.strip).count == 0
			discount_id = generate_id('DC',Discount)
			discount = Discount.new do |d|
				d.id = discount_id
				d.name = params[:name].titleize.strip
				d.description = params[:description].titleize.strip
				d.discount_type = params[:discount_type]
				d.amount = params[:amount]
			end
      if discount.save
        render json: {status: "success"}.as_json
      end
		else
			render(json: {status: "duplicate",message:
        "Duplicate Discount Name. There's a Discount Record with the same name"}.as_json)
		end
  end
  def update
		#if has the same name
		if Discount.where(name: params[:name].
        titleize.strip).where.not(id: params[:id]).count == 0
			discount = Discount.find_by id: params[:id]
			discount.name = params[:name].titleize.strip
			discount.description = params[:description].titleize.strip
			discount.discount_type = params[:discount_type]
			discount.amount = params[:amount]
      if discount.save
        render json: {status: "success"}.as_json
      end
		else
			render(json: {status: "duplicate",
				message: "Duplicate Discount Name. There's a Discount Record with the same name"}.as_json)
		end
  end
  def destroy
		discount = Discount.find_by id: params[:id]
		discount.status = 0
		if discount.save
			render json: {status: "success"}.as_json
		else
			render json: {status: "error"}.as_json
		end
  end

  def records
    render json: Discount.records.as_json
  end

  def search
    s = params[:search]
    w = "%#{s}%"
    records = Discount.where("status = 1 AND (
    (name LIKE ?) OR (description LIKE ?) OR (discount_type LIKE ?) OR (amount LIKE ?)
    )",w,w,w,w)
    render json: records.as_json
  end
end

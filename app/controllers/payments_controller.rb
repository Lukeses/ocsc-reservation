class PaymentsController < ApplicationController

  before_action :naka_login_ba?

	before_action :set_payment, only: [
    :all_payment,
    :all_payments,
    :total_payment,
    :total_payments
  ]

  after_action :paid_naba?, only: :create

  def all_payment
	 	render json: @payment.as_json
  end

  def all_payments
	 	render json: Payment.where(status1: 1).as_json
  end

  def total_payment
    py = @payment.sum(:amount)
    render json: py.as_json
  end

  def total_payments
    py = Payment.where(status1:1).sum(:amount)
    render json: py.as_json
  end

  def create
		id = generate_id('PY',Payment)
    py = Payment .new do |f|
      f.id = id
      f.booking_id = params[:booking_id]
      f.trans_date = Date.today()
      f.amount = params[:amount]
    end
    if py.save
      render json: {status: "success"}.as_json
    else 
      render json: {status: "error", error: py.errors}.as_json
    end
  end

  private
    def set_payment
      @payment = Payment.joins(:booking).
      where("bookings.id=? AND bookings.status1=1 AND bookings.status2!=2",params[:booking_id]).
      where(status: 1)
    end

    def paid_naba? 
      #get total amount paid
      total_amount_paid = Payment.joins(:booking).
      where("bookings.id=? AND bookings.status1=1 ",params[:booking_id]).
      where(status: 1).sum(:amount)
      #get reservation details
      res  = Booking.find params[:booking_id]
      #calculate total from reservation details
      total = res[:total] - res[:d_rate]
      #get balance subtracting total from total amount paid
      balance = total.to_f - total_amount_paid.to_f
      #condition if balance left is zero, set to fully paid
      if balance == 0 
        res.status3 = 2
        res.save
      end
    end

end

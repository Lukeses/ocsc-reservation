class PropertiesController < ApplicationController
  before_action :naka_login_ba?

  def index
  end

  def records
    render json: Property.records.as_json
  end

  def create
		#if has the same name
		if Property.where(name: params[:name].titleize.strip).count == 0
			property_id = generate_id('PR',Property)
			property = Property.new do |t|
				t.id = property_id
				t.name = params[:name].titleize.strip
				t.rate = params[:rate]
				t.property_type = params[:property_type]
				t.capacity = params[:capacity]
			end
      if property.save
        render json: {status: "success"}.as_json
      end
		else
			render(json: {status: "duplicate",message:
        "Duplicate Property Name. There's a Record with the same name"}.as_json)
		end
  end

  def update
		#if has the same name
		if Property.where(name: params[:name].
        titleize.strip).where.not(id: params[:id]).count == 0
			property = Property.find_by id: params[:id]
			property.name = params[:name].titleize.strip
			property.rate = params[:rate]
			property.property_type = params[:property_type]
			property.capacity = params[:capacity]
      if property.save
        render json: {status: "success"}.as_json
      end
		else
			render(json: {status: "duplicate",
				message: "Duplicate Property Name. There's a Record with the same name"}.as_json)
		end
  end

  def destroy
		property = Property.find_by id: params[:id]
		property.status = 0
		if property.save
			render json: {status: "success"}.as_json
		else
			render json: {status: "error"}.as_json
		end
  end

  def search
    s = params[:search]
    w = "%#{s}%"
    records = Property.where("status = 1 AND (
      (id LIKE ?) OR (name LIKE ?) OR (rate LIKE ?) OR
      (property_type LIKE ?) OR (capacity LIKE ?)
    )",w,w,w,w,w)
    render json: records.as_json
  end
  
end

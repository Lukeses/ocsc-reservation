class RentalServicesController < ApplicationController

  before_action :naka_login_ba?

  def create 
    #Generate ID
    id = generate_id('RS', RentalService)
    rs = RentalService.new do |f|
      f.id = id
      f.description = params[:description].titleize.strip
      f.rate = params[:rate]
    end
    if rs.save
      render json: {status: "success", id: id, text: "#{params[:description]} (#{params[:rate]})" }.as_json
    else 
      render json: {status: "error", error: rs.errors}.as_json
    end
  end

end

class ReservationsController < ApplicationController

  before_action :naka_login_ba?

  include ReservationsHelper
  before_action :selected_from_calendar?, only: :new
  before_action :voucher_has_booking_id?, only: :voucher
  before_action :set_reservation, only: [
    :checkin,
    :checkout,
    :cancel
  ]


  def index
  end

  def records
    record = Booking.records
    render json: record.as_json
  end

  def filter
    #filters checkin date
    record = Booking.records
    record = record.where(checkin: params[:checkin])
    render json: {records: record, status: "success"} .as_json
  end

  def search
    s = "%#{params[:search]}%"
    record = Booking.records
    record = record.having(" 
      (id LIKE ?) OR
      (name LIKE ?) OR
      (transportation_name LIKE ?) OR 
      (property_name LIKE ?) OR
      (source_name LIKE ?) OR
      (checkin LIKE ?) OR
      (checkout LIKE ?) OR
      (payment LIKE ?) OR
      (balance LIKE ?) 
    ", s,s,s,s,s,s,s,s,s)
    render json: record.as_json
  end

  def create
    id = generate_id('BK',Booking)
    #for new guest, generate new id and save
    if params[:client_mode] == "new"
      #if has the same name
      if Client.where(name: params[:name].titleize.strip).count == 0
        client_id = generate_id('CL',Client)
        Client.create do |c|
          c.id = client_id
          c.name = params[:name].titleize.strip
          c.company = params[:company].titleize.strip
          c.email = params[:email].strip
          c.contact = params[:contact].strip
        end
      else
        render(:json=> {status: "duplicate",
                        message: "Duplicate Guest Name. There's a
            registered Guest with the same name"}.as_json) and return
      end
      #for registered guest, store client_id
    elsif params[:client_mode] == "existing"
      client_id = params[:client_id]
    end

    @booking = Booking.new do |c|
      c.id = id
      c.client_id = client_id
      c.transportation_id = (params[:transportation] == "none" ? nil : params[:transportation])
      c.property_id = params[:property][:id]
      c.discount_id = (params[:discount] == "none" ? nil : params[:discount])
      c.source_id = params[:source]
      c.rental_service_id = params[:rental_services]
      c.checkin = params[:checkin].to_datetime
      c.checkout = params[:checkout].to_datetime
      c.no_of_person = params[:no_of_person]
      c.no_of_senior_citizen = params[:no_of_senior_citizen]
      c.p_rate = (params[:property][:rate].to_f * calcNights(params[:checkin].to_date, params[:checkout].to_date))
      c.rs_rate = calcRentalServices(params[:rental_services])
      c.t_rate = calcTranspoRate c.transportation_id
      c.total = 0 + (c.p_rate + c.rs_rate + c.t_rate)
      c.d_rate = calcDiscountRate c.discount_id, c.total, c.no_of_person, c.no_of_senior_citizen
      c.note = params[:note]
      #c.description = params[:f_description].titleize.strip
    end
    if @booking.save
      render(:json=> {status:"success", id: id}.as_json) and return
    else
      render(:json=> {status:"error", id: id, error:@booking.errors}.as_json) and return
    end
    # end
  end

  def calcDiscountRate(id, total, nop, nosc)
    if id.nil?
      return 0
    else
      #special case for senior_citizen discount
      if id == "senior_citizen"
        no_of_guest = nop + nosc
        d_rate = total.to_f / no_of_guest.to_f
        d_rate *=  nosc.to_f
        discount = Discount.select(:amount, :discount_type).where(id: id)
        return d_rate.to_f * (discount[0][:amount]/100)
      else
        discount = Discount.select(:amount, :discount_type).where(id: id)
        return (discount[0][:amount]/100) * total
      end
    end
  end

  def calcTranspoRate(id)
    t_rate = 0
    if id.nil?
      return 0
    else
      transpo = Transportation.select(:rate).where(id: id)
      return transpo[0][:rate]
    end
  end

  def calcRentalServices(rs_arr)
    rs_rate = 0
    if rs_arr.nil?
      return 0
    else
      rs_arr.each do |rs|
        rental_service = RentalService.select(:rate).where(id: rs)
        rs_rate += rental_service[0][:rate]
      end
      return rs_rate
    end
  end

  def calcNights(checkin, checkout)
    nights = checkout - checkin
    return nights.to_i
  end

  def new
    @sources = Source.records
    @transportation = Transportation.records
    @discount = Discount.records
    @property = Property.records
    @rental_service = RentalService.records
    @client = Client.records
    @reserve = {
      day: (params[:day] ? sprintf("%02d", params[:day]) : nil),
      month: (params[:month] ? sprintf("%02d", params[:month]) : nil),
      year: (params[:year] ? params[:year] : nil),
      property_id: params[:property_id]
    }
  end

  def checkin
    @reservation.status2 = 2
    @reservation.save
  end

  def checkout
    @reservation.status2 = 3
    @reservation.save
  end

  def cancel
    @reservation.status2 = 0
    if @reservation.save
      render :json=> {status:"success"}.as_json
    else
      render :json=> {status:"error", error:@reservation.errors}.as_json
    end
  end

  def voucher
    @record = Booking.record params[:booking_id]
    @record = @record[0]
  end

  private

    def selected_from_calendar?
      if(!(params[:day] && params[:month] && params[:year] && params[:property_id]))
        redirect_to bookings_path
      end
    end

    def voucher_has_booking_id?
      if(!params[:booking_id])
        redirect_to reservations_path
      end
    end

    def set_reservation
      @reservation = Booking.find_by id: params[:id]
    end

end

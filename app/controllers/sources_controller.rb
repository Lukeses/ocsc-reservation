class SourcesController < ApplicationController
  before_action :naka_login_ba?
  def index
  end
  def create
		#if has the same name
		if Source.where(name: params[:name].titleize.strip).count == 0
			source_id = generate_id('SRC',Source)
			source = Source.new do |s|
				s.id = source_id
				s.name = params[:name].titleize.strip
        s.color = params[:color]
			end
      if source.save
        render json: {status: "success"}.as_json
      end
		else
			render(json: {status: "duplicate",message:
        "Duplicate Source Name. There's a Source Record with the same name"}.as_json)
		end
  end
  def update
		#if has the same name
		if Source.where(name: params[:name].
        titleize.strip).where.not(id: params[:id]).count == 0
			source = Source.find_by id: params[:id]
			source.name = params[:name].titleize.strip
      source.color = params[:color]
      if source.save
        render json: {status: "success"}.as_json
      end
		else
			render(json: {status: "duplicate",
				message: "Duplicate Source Name. There's a Source Record with the same name"}.as_json)
		end
  end
  def destroy
		source = Source.find_by id: params[:id]
		source.status = 0
		if source.save
			render json: {status: "success"}.as_json
		else
			render json: {status: "error"}.as_json
		end
  end

  def records
    render json: Source.records.as_json
  end

  def search
    s = params[:search]
    records = Source.where("status = 1 AND (name LIKE ?)","%#{s}%")
    render json: records.as_json
  end
end

class TransportationsController < ApplicationController
  before_action :naka_login_ba?
  def index
  end

  def records
    render json: Transportation.records.as_json
  end

  def create
		#if has the same name
		if Transportation.where(description: params[:description].titleize.strip).count == 0
			transportation_id = generate_id('TR',Transportation)
			transportation = Transportation.new do |t|
				t.id = transportation_id
				t.description = params[:description].titleize.strip
				t.rate = params[:rate]
			end
      if transportation.save
        render json: {status: "success"}.as_json
      end
		else
			render(json: {status: "duplicate",message:
        "Duplicate Transportation Name. There's a Record with the same name"}.as_json)
		end
  end

  def update
		#if has the same name
		if Transportation.where(description: params[:description].
        titleize.strip).where.not(id: params[:id]).count == 0
			transportation = Transportation.find_by id: params[:id]
			transportation.description = params[:description].titleize.strip
			transportation.rate = params[:rate]
      if transportation.save
        render json: {status: "success"}.as_json
      end
		else
			render(json: {status: "duplicate",
				message: "Duplicate Transportation Name. There's a Record with the same name"}.as_json)
		end
  end

  def destroy
		transportation = Transportation.find_by id: params[:id]
		transportation.status = 0
		if transportation.save
			render json: {status: "success"}.as_json
		else
			render json: {status: "error"}.as_json
		end
  end

  def search
    s = params[:search]
    w = "%#{s}%"
    records = Transportation.where("status = 1 AND (
      (description LIKE ?) OR (rate LIKE ?)
    )",w,w)
    render json: records.as_json
  end
end

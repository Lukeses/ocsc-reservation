module ApplicationHelper

	def render_html(element)
		return element.html_safe
	end

	def render_json(element)
		return element.as_json
	end

  def generate_id(prefix,model)
		# 2 Paramenters
		#1st is for the Prefix which will be for the name of ID
		#2nd is the Model which will it generate id FROM WHAT Table/Model
		id = prefix+Time.new.year.to_s.slice(2,3)+"-"
		count = sprintf "%07d", model.count.to_s
		id+= count                 
		return id
	end 

  def naka_login_ba?
    if !user_signed_in?
      redirect_to new_user_session_path
    end
  end

  def format_number(num) 
    number_with_precision num, precision: 2, delimiter:',' 
  end

  def peso(num)
    "PHP #{format_number num}"
  end

  def dmY(date) 
    date.strftime "%m/%d/%Y"
  end

end

class Booking < ApplicationRecord
	#validations
	validate :no_conflict?, on: :create

	#foreign keys
	belongs_to :client
	belongs_to :transportation, optional:true
	belongs_to :property
	belongs_to :discount, optional: true
	belongs_to :source
	belongs_to :rental_service, optional: true

	has_many :payments


	def self.records
		return  self.select(@query).joins(:client, :property, :source).
		left_joins(:rental_service, :discount, :transportation,:payments).
    where("(status1!=-0 AND status2!=0)  AND (status2!=3  OR status3!=2)").
    group("bookings.id").
    order("bookings.created_at DESC")
	end

	def self.record(id)
		return  self.select(@query).joins(:client, :property, :source).
		left_joins(:rental_service, :discount, :transportation,:payments).
    where("(status1!=-0 AND status2!=0)  AND (status2!=3  OR status3!=2)").
		where("bookings.id = ?", id)
    group("bookings.id")
    order(checkin: :asc)
	end

	def self.recordsByDate(y,m)
		return  self.select(@query).joins(:client, :property, :source).
		left_joins(:rental_service, :discount, :transportation, :payments).where(status1: 1).
		where("YEAR(bookings.checkin)=? AND MONTH(bookings.checkin)=?", y,m).
    where("(status1!=-0 AND status2!=0)  AND (status2!=3)").
    group("bookings.id")
    order(checkin: :asc)
	end

	def self.check_duplicate(p_id,check_in)
		return self.where(status1: 1,
		property_id: p_id,
    checkin: check_in).
    where.not(status2: 0).where.not(status2: 3).count
	end

	def self.next_book(p_id, check_in, check_out)
		upcoming = Booking.select(:checkin).
		where(property_id: p_id).
		where('checkin > ?', check_in).
    where(status1: 1).
    where.not(status2: 0).where.not(status2: 3).
    order(:checkin).first
		if upcoming.nil?
			return false
		else
			unless check_out <= upcoming[:checkin]
				return true
			else
				return false
			end
		end
	end
  private
		@query = "bookings.id as id,
		clients.name as name,
		transportations.description as transportation_name,
		properties.name as property_name,
		properties.id as property_id,
		sources.name as source_name, 
    sources.color as source_color,
		bookings.checkin,
		bookings.checkout,
		DATEDIFF(checkout, checkin) as nights,
    COALESCE(SUM(payments.amount),0) as payment,
    COALESCE(( (bookings.total-bookings.d_rate) - COALESCE(SUM(payments.amount),0) ),0) as balance,
		bookings.total,
		bookings.p_rate,
		bookings.rs_rate,
		bookings.t_rate,
		bookings.d_rate,
		bookings.status1,
		bookings.status2,
		bookings.status3"

		def no_conflict?
			if Booking.check_duplicate(property_id, checkin) > 0
				errors.add :checkin, "Check-In Date is already booked"
			elsif checkout < checkin
				errors.add :checkout, "Check-Out must not be a day before the Check-In Date"
			elsif Booking.next_book property_id, checkin, checkout
				errors.add :checkout, "Check-Out is in conflict with another bookings"
			end
		end

end

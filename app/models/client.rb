class Client < ApplicationRecord

	has_many :bookings

  def self.records
    return Client.all.where(status: 1).order('created_at DESC, updated_at DESC')
  end

end

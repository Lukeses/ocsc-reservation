class Discount < ApplicationRecord

	has_many :bookings

  def self.records
    return Discount.all.where(status: 1).order('created_at DESC, updated_at DESC')
  end
end

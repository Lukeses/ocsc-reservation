class Payment < ApplicationRecord
	#foreign keys
	belongs_to :booking
  validates :amount, :booking_id, presence: true
  validates :amount, numericality: {greater_than: 0}
end

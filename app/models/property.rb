class Property < ApplicationRecord

	has_many :bookings

  def self.records
    return self.all.where(status: 1).order('created_at DESC, updated_at DESC')
  end

  def self.rooms
    return self.all.
		where(status: 1, property_type: 'room').
		order('created_at DESC, updated_at DESC')
  end
  def self.cottages
    return self.all.
		where(status: 1, property_type: 'cottage').
		order('created_at DESC, updated_at DESC')
  end
  def self.events
    return self.all.
		where(status: 1, property_type: 'event').
		order('created_at DESC, updated_at DESC')
  end
end

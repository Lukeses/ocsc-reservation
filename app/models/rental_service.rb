class RentalService < ApplicationRecord

	has_many :bookings
  validates :description, :rate, presence: true
  validates :rate, numericality: {greater_than: 0}

  def self.records
    return RentalService.all.where status: 1
  end
end

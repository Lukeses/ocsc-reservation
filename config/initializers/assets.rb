# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w(
pageplugin/reservations/reservations.js.erb
pageplugin/reservations/new.js.erb
pageplugin/reservations/show.js.erb
pageplugin/reservations/index.js.erb
pageplugin/bookings/bookings.js.erb
pageplugin/bookings/index.js.erb
pageplugin/bookings/index.scss
pageplugin/dashboard/index.js.erb
pageplugin/reservations/main.css
pageplugin/reservations/normalize.css
pageplugin/reservations/paper.css
)

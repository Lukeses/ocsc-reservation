Rails.application.routes.draw do

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root "bookings#index"

	resources :reservations do
		collection do
			post :records
      post :filter
      post :search
      post :checkin
      post :checkout
      post :cancel
      get :voucher
		end
	end
  get 'bookings/rooms', to: 'bookings#index'

  resources :bookings, only: [:index, :create, :update, :destroy, :show] do
		collection do
			post :bookingsByDate
      get :cottages
      get :events
		end
	end

  resources :clients, only: [:index, :create, :update, :destroy] do
    collection do
      post :records
      post :search
    end
  end

  resources :sources, only: [:index, :create, :update, :destroy] do
    collection do
      post :records
      post :search
    end
  end

  resources :discounts, only: [:index, :create, :update, :destroy] do
    collection do
      post :records
      post :search
    end
  end

  resources :transportations, only: [:index, :create, :update, :destroy] do
    collection do
      post :records
      post :search
    end
  end

  resources :properties, only: [:index, :create, :update, :destroy] do
    collection do
      post :records
      post :search
    end
  end

  #Payments
  resources :payments, only: [:create, :update, :destroy] do
		collection do
			post :all_payment
			post :all_payments
			post :total_payment
			post :total_payments
		end
	end

  #Rental Services
  resources :rental_services, only: [:create]

end

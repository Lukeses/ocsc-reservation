class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients, {id:false} do |t|
      t.column :id, 'VARCHAR(30) PRIMARY KEY'
      t.string :name, null:false
      t.string :company, null:true
      t.string :contact, limit:30, null:true
      t.string :email, null:true
      t.integer :status, limit: 1, default:1

      t.index ["name"], name: "index_clients_on_name", unique: true, using: :btree
      t.timestamps
    end
  end
end

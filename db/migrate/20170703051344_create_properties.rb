class CreateProperties < ActiveRecord::Migration[5.0]
  def change
    create_table :properties,{id:false} do |t|
      t.column :id, 'varchar(30) PRIMARY KEY'
      t.string :name, null:false
      t.decimal :rate, null:false
      t.string :type, null:false
      t.integer :capacity, null:true
      t.integer :status, limit: 1, default:1

      t.index ["name"], name: "index_properties_on_name", unique: true, using: :btree
      t.timestamps
    end
  end
end

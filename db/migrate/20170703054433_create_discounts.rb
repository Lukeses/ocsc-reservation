class CreateDiscounts < ActiveRecord::Migration[5.0]
  def change
    create_table :discounts, {id: false} do |t|

      t.column :id, 'VARCHAR(30) PRIMARY KEY'
      t.string :name, unique:true
      t.string :description, null:true
      t.string :discount_type
      t.decimal :amount, precision:9, scale:2
      t.integer :status, limit: 1, default:1
      #index
      t.index ["name"], name: "index_discounts_on_name", unique: true, using: :btree

      t.timestamps
    end
  end
end

class CreateTransportations < ActiveRecord::Migration[5.0]
  def change
    create_table :transportations, {id: false} do |t|
      t.column :id, 'varchar(30) PRIMARY KEY'
      t.string :description, null:false
      t.decimal :rate, precision:9, scale:2
      t.integer :status, limit: 1, default:1
      t.timestamps
    end
  end
end

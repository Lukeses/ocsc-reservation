class CreateSources < ActiveRecord::Migration[5.0]
  def change
    create_table :sources, {id:false} do |t|
      t.column :id, 'varchar(30) PRIMARY KEY'
      t.string :name, null:false
      t.integer :status, limit: 1, default:1
      #index
      t.index ["name"], name: "index_sources_on_name", unique: true, using: :btree
      t.timestamps
    end
  end
end

class RenameTypeToPropertyTypetoProperty < ActiveRecord::Migration[5.0]
  def up
    rename_column :properties, :type, :property_type
  end
  def down
    rename_column :properties, :property_type, :type
  end
end

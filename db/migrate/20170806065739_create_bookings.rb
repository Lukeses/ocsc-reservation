class CreateBookings < ActiveRecord::Migration[5.0]
  def change
    create_table :bookings, {id: false} do |t|
      t.column :id, 'varchar(30) PRIMARY KEY'
			t.string :client_id, limit: 30, null: false
			t.string :transportation_id, limit: 30, null: false
			t.string :property_id, limit: 30, null: false
			t.string :discount_id, limit: 30, null: false
			t.string :source_id, limit: 30, null: false
			t.string :rental_service_id, null: true
			t.datetime :checkin, null: false
			t.datetime :checkout, null: false
			t.integer :no_of_person, null: false
			t.decimal :p_rate, null:false, precision:9, scale:2
			t.decimal :rs_rate, null:false, precision:9, scale:2
			t.decimal :total, null:false, precision:9, scale:2
			t.integer :status1, limit:1, default:1
			t.integer :status2, limit:1, default:1
			t.integer :status3, limit:1, default:1
      t.timestamps
      t.index ["checkin"], name: "index_booking_on_checkin", unique: true, using: :btree
    end
  end
end

class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments, {id: false} do |t|
			t.column :id, "VARCHAR(30) PRIMARY KEY"
			t.string :booking_id, limit: 30, null:false
			t.string :ref_id, limit: 50, null:true
			t.datetime :trans_date, null:false
      t.string :description, null:true
			t.decimal :amount, precision:9, scale:2, null:false
			t.string :payment_type, limit: 50, default:'cash'
			t.integer :status, limit: 1, default:1
			#index
			t.index ["ref_id"], name: "index_ref_id_on_payments", unique: true, using: :btree
			t.index ["id"], name: "index_id_on_payments", unique: true, using: :btree


      t.timestamps
    end
  end
end

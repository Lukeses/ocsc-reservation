class ChangeDiscountNullToBookings < ActiveRecord::Migration[5.0]
  def up
		change_column_null :bookings, :discount_id, true
  end
	def down 
		change_column_null :bookings, :discount_id, false
	end
end

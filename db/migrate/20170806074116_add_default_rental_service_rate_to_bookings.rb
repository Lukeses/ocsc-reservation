class AddDefaultRentalServiceRateToBookings < ActiveRecord::Migration[5.0]
  def change
		change_column_default :bookings, :rs_rate, 0
  end
end

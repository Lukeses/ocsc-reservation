class RemoveIndexToBookings < ActiveRecord::Migration[5.0]
  def change
		remove_index :bookings, :checkin
  end
end

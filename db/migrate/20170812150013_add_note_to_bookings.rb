class AddNoteToBookings < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :note, :string, null: true
  end
end

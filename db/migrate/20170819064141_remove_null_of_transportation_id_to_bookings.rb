class RemoveNullOfTransportationIdToBookings < ActiveRecord::Migration[5.0]
  def up
    change_column_null :bookings, :transportation_id, true
  end
  def down
    change_column_null :bookings, :transportation_id, false
  end
end

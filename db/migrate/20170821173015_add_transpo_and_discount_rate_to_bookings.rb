class AddTranspoAndDiscountRateToBookings < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :t_rate, :decimal, null: true
    add_column :bookings, :d_rate, :decimal, null: true
  end
end

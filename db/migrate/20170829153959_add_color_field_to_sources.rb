class AddColorFieldToSources < ActiveRecord::Migration[5.0]
  def change
    add_column :sources, :color, :string, null: false, limit: 7
  end
end

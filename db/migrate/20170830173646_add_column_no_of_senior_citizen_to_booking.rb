class AddColumnNoOfSeniorCitizenToBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :no_of_senior_citizen, :integer, null: true, default: 0
  end
end

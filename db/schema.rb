# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170830173646) do

  create_table "bookings", id: :string, limit: 30, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "client_id",            limit: 30,                                          null: false
    t.string   "transportation_id",    limit: 30
    t.string   "property_id",          limit: 30,                                          null: false
    t.string   "discount_id",          limit: 30
    t.string   "source_id",            limit: 30,                                          null: false
    t.string   "rental_service_id"
    t.datetime "checkin",                                                                  null: false
    t.datetime "checkout",                                                                 null: false
    t.integer  "no_of_person",                                                             null: false
    t.decimal  "p_rate",                          precision: 9,  scale: 2,                 null: false
    t.decimal  "rs_rate",                         precision: 9,  scale: 2, default: "0.0", null: false
    t.decimal  "total",                           precision: 9,  scale: 2,                 null: false
    t.integer  "status1",              limit: 1,                           default: 1
    t.integer  "status2",              limit: 1,                           default: 1
    t.integer  "status3",              limit: 1,                           default: 1
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.string   "note"
    t.decimal  "t_rate",                          precision: 10
    t.decimal  "d_rate",                          precision: 10
    t.integer  "no_of_senior_citizen",                                     default: 0
  end

  create_table "clients", id: :string, limit: 30, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name",                              null: false
    t.string   "company"
    t.string   "contact",    limit: 30
    t.string   "email"
    t.integer  "status",     limit: 1,  default: 1
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["name"], name: "index_clients_on_name", unique: true, using: :btree
  end

  create_table "discounts", id: :string, limit: 30, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "description"
    t.string   "discount_type"
    t.decimal  "amount",                  precision: 9, scale: 2
    t.integer  "status",        limit: 1,                         default: 1
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.index ["name"], name: "index_discounts_on_name", unique: true, using: :btree
  end

  create_table "payments", id: :string, limit: 30, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "booking_id",   limit: 30,                                          null: false
    t.string   "ref_id",       limit: 50
    t.datetime "trans_date",                                                       null: false
    t.string   "description"
    t.decimal  "amount",                  precision: 9, scale: 2,                  null: false
    t.string   "payment_type", limit: 50,                         default: "cash"
    t.integer  "status",       limit: 1,                          default: 1
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.index ["id"], name: "index_id_on_payments", unique: true, using: :btree
    t.index ["ref_id"], name: "index_ref_id_on_payments", unique: true, using: :btree
  end

  create_table "properties", id: :string, limit: 30, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name",                                               null: false
    t.decimal  "rate",                    precision: 10,             null: false
    t.string   "property_type",                                      null: false
    t.integer  "capacity"
    t.integer  "status",        limit: 1,                default: 1
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.index ["name"], name: "index_properties_on_name", unique: true, using: :btree
  end

  create_table "rental_services", id: :string, limit: 30, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "description",                                               null: false
    t.decimal  "rate",                  precision: 9, scale: 2
    t.integer  "status",      limit: 1,                         default: 1
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
  end

  create_table "sources", id: :string, limit: 30, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name",                             null: false
    t.integer  "status",     limit: 1, default: 1
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "color",      limit: 7,             null: false
    t.index ["name"], name: "index_sources_on_name", unique: true, using: :btree
  end

  create_table "transportations", id: :string, limit: 30, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "description",                                               null: false
    t.decimal  "rate",                  precision: 9, scale: 2
    t.integer  "status",      limit: 1,                         default: 1
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email",                  default: "", null: false
    t.string   "fullname",               default: ""
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Users
User.create email: "admin@catanduanestwinrock.com", fullname: "Administrator", password: "admin1234"

#Discount
Discount.create([ 
  {id: "affiliate", name: "Affiliate Discount", amount: 40, discount_type: "percentage"},
  {id: "senior_citizen", name: "Senior Citizen", amount: 20, discount_type: "percentage"}
])

#Source
Source.create([ 
  {id: "walkin", name: "Walk-In", color: "#EAC435"},
  {id: "agoda", name: "Agoda", color: "#2DCCD3"},
  {id: "web", name: "Web", color: "#E40066"}
])

#Properties
Property.create([ 
  {id: 1, name: "Sharon", rate: 2000, property_type: "room", capacity: 2},
  {id: 2, name: "Sabrina", rate: 2500, property_type: "room", capacity: 3},
  {id: 3, name: "Noella", rate: 2500, property_type: "room", capacity: 3},
  {id: 4, name: "Deluxe Room 1", rate: 1650, property_type: "room", capacity: 2},
  {id: 5, name: "Deluxe Room 2", rate: 1650, property_type: "room", capacity: 3},
  {id: 6, name: "Deluxe Room 3", rate: 3500, property_type: "room", capacity: 2},
  {id: 7, name: "Deluxe Room 4", rate: 3500, property_type: "room", capacity: 2},
  {id: 8, name: "Deluxe Room 5", rate: 1650, property_type: "room", capacity: 3},
  {id: 9, name: "Deluxe Room 6", rate: 1650, property_type: "room", capacity: 3},
  {id: 10, name: "Deluxe Room 7", rate: 1650, property_type: "room", capacity: 3},
  {id: 11, name: "Deluxe Room 8", rate: 1650, property_type: "room", capacity: 2},
  {id: 12, name: "Deluxe Room 9", rate: 1650, property_type: "room", capacity: 2},
  {id: 13, name: "Deluxe Room 10", rate: 3000, property_type: "room", capacity: 6},
  {id: 14, name: "Suite Room", rate: 3000, property_type: "room", capacity: 2},
  {id: 15, name: "Dorm UP", rate: 3000, property_type: "room", capacity: 12},
  {id: 16, name: "Dorm Down", rate: 1000, property_type: "room", capacity: 28},
  {id: 17, name: "Nipa Rock Room 3", rate: 1350, property_type: "room", capacity: 3},
  {id: 18, name: "Nipa Rock Room 4", rate: 1000, property_type: "room", capacity: 3},
  {id: 19, name: "Nipa Rock Room 5", rate: 1350, property_type: "room", capacity: 3},
  {id: 20, name: "Nipa Rock Room 6", rate: 1000, property_type: "room", capacity: 3},
  {id: 21, name: "Close Cottage 2A", rate: 600, property_type: "room", capacity: 2},
  {id: 22, name: "Close Cottage 2B", rate: 600, property_type: "room", capacity: 2},
  {id: 23, name: "Thea", rate: 4000, property_type: "room", capacity: 4},
  {id: 24, name: "Aira", rate: 4000, property_type: "room", capacity: 4},
  {id: 25, name: "Celina", rate: 2500, property_type: "room", capacity: 3},
  {id: 26, name: "Lawn Cottage Regular #3", rate: 300, property_type: "cottage", capacity: 6},
  {id: 27, name: "Lawn Cottage Regular #4", rate: 300, property_type: "cottage", capacity: 6},
  {id: 28, name: "Lawn Cottage Regular #1", rate: 350, property_type: "cottage", capacity: 8},
  {id: 29, name: "Lawn Cottage Regular #5", rate: 350, property_type: "cottage", capacity: 8},
  {id: 30, name: "Lawn Cottage Regular #6", rate: 350, property_type: "cottage", capacity: 8},
  {id: 31, name: "Lawn Cottage Regular #7", rate: 350, property_type: "cottage", capacity: 8},
  {id: 32, name: "Lawn Cottage Regular #10", rate: 350, property_type: "cottage", capacity: 8},
  {id: 33, name: "Lawn Cottage Regular #12", rate: 350, property_type: "cottage", capacity: 8},
  {id: 34, name: "Lawn Cottage Regular #13", rate: 350, property_type: "cottage", capacity: 8},
  {id: 35, name: "Lawn Cottage Regular #8", rate: 400, property_type: "cottage", capacity: 10},
  {id: 36, name: "Lawn Cottage Big #2", rate: 500, property_type: "cottage", capacity: 15},
  {id: 37, name: "Lawn Cottage Big #9", rate: 500, property_type: "cottage", capacity: 15},
  {id: 38, name: "Bench Beachfront", rate: 150, property_type: "cottage", capacity: 4},
  {id: 39, name: "Loverslane #1", rate: 250, property_type: "cottage", capacity: 5},
  {id: 40, name: "Loverslane #2", rate: 250, property_type: "cottage", capacity: 5},
  {id: 41, name: "Loverslane #3", rate: 250, property_type: "cottage", capacity: 5},
  {id: 42, name: "Loverslane #4", rate: 250, property_type: "cottage", capacity: 5},
  {id: 43, name: "Loverslane #5", rate: 250, property_type: "cottage", capacity: 5},
  {id: 44, name: "Loverslane #6", rate: 250, property_type: "cottage", capacity: 5},
  {id: 45, name: "Loverslane #7", rate: 250, property_type: "cottage", capacity: 5}
])

#!/usr/bin/expect -f

# Get a Bash shell
spawn -noecho bash

# Wait for a prompt
expect "$ "

# Type something
send "ocsc-reservation && bundler && rails db:migrate  && rails restart && rails s -p 1234
"

# Hand over control to the user
interact

exit

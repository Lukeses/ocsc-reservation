#!/usr/bin/expect -f

# Get a Bash shell
spawn -noecho bash

# Wait for a prompt
expect "$ "

# Type something
send "ocsc-reservation && git stash && git clean -fxd && git checkout master && git pull origin master && exit
"

# Hand over control to the user
interact

exit

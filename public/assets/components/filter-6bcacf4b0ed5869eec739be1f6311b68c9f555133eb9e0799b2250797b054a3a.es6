var Filter = function (props) {
  return React.createElement(
    "div",
    null,
    React.createElement(
      "div",
      { className: "col-sm-4" },
      React.createElement(
        "button",
        {
          onClick: props.filter,
          className: "btn btn-primary" },
        React.createElement("i", { className: "ti-filter" }),
        " Filter"
      )
    )
  );
};
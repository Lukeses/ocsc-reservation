var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Modal = (function (_React$Component) {
  _inherits(Modal, _React$Component);

  function Modal(props) {
    _classCallCheck(this, Modal);

    _get(Object.getPrototypeOf(Modal.prototype), "constructor", this).call(this, props);
    this.renderMain = this.renderMain.bind(this);
  }

  /*
    use this modal when
    something need to become a modal
    specially for form_reservations_new
  
    Instructions:
    1. In parent component, declare this variable in constructor:
  
      this.modal = {
        id: "modal_clients",
        size: "modal-md",
        icon: "",
        title: "Client / Guest"
      }
  
    2. In parent component, call this component with the props modal:
  
      <Modal modal="this.props.modal"></Modal>
  
    3. In parent component, Put your form or you want to become modal inside
    the Modal Component, Like this:
  
      <Modal modal="this.props.modal">
        //Something
        <table>
        </table>
      </Modal>
  
    4. Done, if needed a button in modal, use ModalSaveUpdate Component
  */

  _createClass(Modal, [{
    key: "renderMain",
    value: function renderMain() {
      var modal = this.props.modal;
      return React.createElement(
        "div",
        { id: modal.id, className: "modal fade" },
        React.createElement(
          "div",
          { className: "modal-dialog " + modal.size },
          React.createElement(
            "div",
            { className: "modal-content" },
            React.createElement(
              "div",
              { className: "modal-header" },
              React.createElement(
                "button",
                { type: "butt on",
                  className: "close",
                  "data-dismiss": "modal",
                  "aria-hidden": "true" },
                "×"
              ),
              React.createElement(
                "h4",
                { className: "modal-title" },
                React.createElement("i", { className: modal.icon }),
                "  ",
                modal.title
              )
            ),
            React.createElement(
              "div",
              { className: "modal-body" },
              React.createElement(
                "div",
                { id: "div_modal_form", className: "row" },
                React.createElement(
                  "div",
                  { className: "col-xs-12" },
                  React.createElement(
                    "div",
                    { className: "content" },
                    this.props.children
                  )
                )
              )
            )
          )
        )
      );
    }
  }, {
    key: "render",
    value: function render() {
      return this.renderMain();
    }
  }]);

  return Modal;
})(React.Component);
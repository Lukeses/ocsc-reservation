var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ModalSaveUpdate = (function (_React$Component) {
  _inherits(ModalSaveUpdate, _React$Component);

  function ModalSaveUpdate(props) {
    _classCallCheck(this, ModalSaveUpdate);

    _get(Object.getPrototypeOf(ModalSaveUpdate.prototype), 'constructor', this).call(this, props);
    this.renderSaveUpdate = this.renderSaveUpdate.bind(this);
    this.renderMain = this.renderMain.bind(this);
    this.update = this.update.bind(this);
    this.save = this.save.bind(this);
  }

  _createClass(ModalSaveUpdate, [{
    key: 'update',
    value: function update() {
      document.getElementById('modal-update').disabled = true;
      this.props.update(this.props.mode, function () {
        document.getElementById('modal-update').disabled = false;
      });
    }
  }, {
    key: 'save',
    value: function save() {
      document.getElementById('modal-save').disabled = true;
      this.props.save(function () {
        document.getElementById('modal-save').disabled = false;
      });
    }
  }, {
    key: 'renderSaveUpdate',
    value: function renderSaveUpdate() {
      if (this.props.mode == "save") {
        return React.createElement(
          'button',
          { type: 'button',
            onClick: this.save,
            id: 'modal-save',
            className: 'btn btn-info btn btn-fill btn-lg' },
          'SAVE'
        );
      } else {
        return React.createElement(
          'div',
          null,
          React.createElement(
            'button',
            { type: 'button',
              onClick: this.update,
              id: 'modal-update',
              className: 'btn btn-warning btn btn-fill btn-lg' },
            'UPDATE'
          )
        );
      }
    }
  }, {
    key: 'renderMain',
    value: function renderMain() {
      return React.createElement(
        'div',
        null,
        React.createElement('hr', null),
        React.createElement(
          'div',
          { className: 'text-center' },
          this.renderSaveUpdate()
        ),
        React.createElement('div', { className: 'clearfix' })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      return this.renderMain();
    }
  }]);

  return ModalSaveUpdate;
})(React.Component);
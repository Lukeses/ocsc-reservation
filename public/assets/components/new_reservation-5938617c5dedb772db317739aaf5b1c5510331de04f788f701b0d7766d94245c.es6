var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NewReservation = (function (_React$Component) {
  _inherits(NewReservation, _React$Component);

  function NewReservation() {
    _classCallCheck(this, NewReservation);

    _get(Object.getPrototypeOf(NewReservation.prototype), "constructor", this).apply(this, arguments);
  }

  _createClass(NewReservation, [{
    key: "loadOldStartupScripts",
    value: function loadOldStartupScripts() {
      uc.sidebar.menu_name1("reservations_new");
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.loadOldStartupScripts();
    }
  }, {
    key: "render",
    value: function render() {
      var dropdowns = {
        sources: this.props.sources,
        transportation: this.props.transportation,
        discount: this.props.discount,
        property: this.props.property,
        clients: this.props.clients,
        rental_service: this.props.rental_service
      };
      return React.createElement(ReservationForm, { dropdowns: dropdowns,
        reserve: this.props.reserve });
    }
  }]);

  return NewReservation;
})(React.Component);
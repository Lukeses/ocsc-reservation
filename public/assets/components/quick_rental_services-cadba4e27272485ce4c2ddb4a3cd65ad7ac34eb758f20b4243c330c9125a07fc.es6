var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var QuickRentalServices = (function (_React$Component) {
  _inherits(QuickRentalServices, _React$Component);

  function QuickRentalServices(props) {
    _classCallCheck(this, QuickRentalServices);

    _get(Object.getPrototypeOf(QuickRentalServices.prototype), "constructor", this).call(this, props);
    this.clear = this.clear.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.renderMain = this.renderMain.bind(this);
    this.modal = {
      id: "modal_rental_services",
      size: "modal-md",
      icon: "",
      title: "Rental Service"
    };
    this.state = {
      name: '',
      rate: ''
    };
  }

  _createClass(QuickRentalServices, [{
    key: "handleChange",
    value: function handleChange(e) {
      var value = e.target.value;
      var name = e.target.name;
      this.setState(_defineProperty({}, name, value), function () {
        // console.log(this.state[name])
      });
    }
  }, {
    key: "handleSave",
    value: function handleSave() {
      if (this.validate(this.state)) {
        self = this;
        var data = {
          description: this.state.name,
          rate: this.state.rate
        };
        this.refs.btn_save.disabled = true;
        $.ajax({
          url: "/rental_services",
          data: data,
          type: "POST",
          success: function (s) {
            switch (s.status) {
              case "success":
                toastr.success("New Reservation", "Saved");
                self.refs.btn_save.disabled = false;
                $("#" + self.modal.id).modal('hide');
                self.clear();
                self.props.append_rental_service(s.id, s.text);
                break;

              case "error":
                var _loop = function (err) {
                  s.error[err].map(function (message) {
                    toastr.warning(message, err.toUpperCase());
                  });
                  self.refs.btn_save.disabled = false;
                };

                for (var err in s.error) {
                  _loop(err);
                }
                break;
            }
          },
          error: function (xhr, status, error) {
            toastr.error("Something Happended", "Error");
            console.log("xhr: " + xhr.status + ", status: " + status + ", error: " + error);
            self.refs.btn_save.disabled = false;
          }
        });
      }
      return false;
    }
  }, {
    key: "clear",
    value: function clear() {
      this.setState({
        name: '',
        rate: ''
      });
      $('#d_q_name #d_q_rate').removeClass('has-error');
    }
  }, {
    key: "validate",
    value: function validate(f) {
      var v = true;
      //specific validation adding has-error to div wrapper of element
      //name
      if (f.name == '') {
        $('#d_q_name').addClass('has-error');
        v = false;
      } else {
        $('#d_q_name').removeClass('has-error');
      }
      //amount
      if (f.rate == '' || f.rate <= 0 || f.rate >= 90000) {
        $('#d_q_rate').addClass('has-error');
        v = false;
      } else {
        $('#d_q_rate').removeClass('has-error');
      }
      //return validation result
      return v;
    }
  }, {
    key: "renderMain",
    value: function renderMain() {
      return React.createElement(
        "div",
        null,
        React.createElement(
          Modal,
          { modal: this.modal },
          React.createElement(
            "div",
            { className: "row" },
            React.createElement(
              "div",
              { className: "col-sm-12" },
              React.createElement(
                "div",
                { className: "form-group col-sm-8", id: "d_q_name" },
                React.createElement(
                  "label",
                  { className: "control-label" },
                  "Name",
                  React.createElement(
                    "star",
                    null,
                    "*"
                  )
                ),
                React.createElement("input", { className: "form-control",
                  onChange: this.handleChange,
                  value: this.state.name,
                  name: "name",
                  required: "true" })
              ),
              React.createElement(
                "div",
                { className: "form-group col-sm-4", id: "d_q_rate" },
                React.createElement(
                  "label",
                  { className: "control-label" },
                  "Rate",
                  React.createElement(
                    "star",
                    null,
                    "*"
                  )
                ),
                React.createElement("input", { className: "form-control",
                  onChange: this.handleChange,
                  value: this.state.rate,
                  type: "number",
                  name: "rate",
                  required: true })
              )
            )
          ),
          React.createElement("hr", null),
          React.createElement(
            "div",
            { className: "text-center" },
            React.createElement(
              "button",
              {
                ref: "btn_save",
                onClick: this.handleSave,
                className: "btn btn-fill btn-info" },
              React.createElement("i", { className: "ti-plus" }),
              " ADD"
            )
          )
        )
      );
    }
  }, {
    key: "render",
    value: function render() {
      return this.renderMain();
    }
  }]);

  return QuickRentalServices;
})(React.Component);
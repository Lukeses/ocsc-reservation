var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ReservationApp = (function (_React$Component) {
	_inherits(ReservationApp, _React$Component);

	function ReservationApp(props) {
		_classCallCheck(this, ReservationApp);

		_get(Object.getPrototypeOf(ReservationApp.prototype), 'constructor', this).call(this, props);
		var d = new Date();
		var monthName = this.getMonthNames();
		var days = new Date(d.getFullYear(), parseInt(d.getMonth()) + 1, 0);
		this.updateDate = this.updateDate.bind(this);
		this.populateProperties = this.populateProperties.bind(this);
		this.selectedCell = this.selectedCell.bind(this);
		this.state = {
			date: {
				month: d.getMonth(),
				year: d.getFullYear(),
				monthName: monthName[d.getMonth()],
				days: days.getDate()
			},
			properties: this.populateProperties(days.getDate())
		};
	}

	_createClass(ReservationApp, [{
		key: 'updateDate',
		value: function updateDate(m, y) {
			var _this = this;

			var monthName = this.getMonthNames();
			var days = new Date(y, parseInt(m) + 1, 0);
			var new_properties = this.state.properties;
			new_properties.map(function (item, i) {
				item.cells = _this.populateCells(days.getDate());
			});
			this.setState({
				date: {
					month: m,
					year: y,
					monthName: monthName[m],
					days: days.getDate()
				},
				properties: new_properties
			}, function () {
				_this.getAjaxBookingByDate();
			});
		}
	}, {
		key: 'populateCells',
		value: function populateCells(qty) {
			var c = [];
			for (var i = 1; i <= qty; i++) {
				c.push({
					id: i,
					availability: true,
					guest: "",
					transpo: false,
					color: '',
					groupedClass: ''
				});
			}
			return c;
		}
	}, {
		key: 'populateProperties',
		value: function populateProperties(qty) {
			var _this2 = this;

			var p = this.props.property;
			var p2 = [];
			p.map(function (item, i) {
				p2.push({ id: item.id, name: item.name, cells: _this2.populateCells(qty) });
			});
			return p2;
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.getAjaxBookingByDate();
			uc.sidebar.menu_name1(this.props.property_type);
		}
	}, {
		key: 'getMonthNames',
		value: function getMonthNames() {
			return ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		}
	}, {
		key: 'getAjaxBookingByDate',
		value: function getAjaxBookingByDate() {
			var self = this;
			var data_params = {
				year: self.state.date.year,
				month: parseInt(self.state.date.month) + 1
			};
			$.ajax({
				url: '/bookings/bookingsByDate',
				data: data_params,
				type: "POST",
				success: function (s) {
					var newcells = self.state.properties;
					s.map(function (record, i) {
						var checkin = record.checkin;
						var checkout = record.checkout;
						checkin = new Date(checkin);
						checkout = new Date(checkout);
						var day = checkin.getDate() - 1;
						var end = checkout.getDate() - 1;
						newcells.map(function (c, i) {
							//add color
							if (c.id == record.property_id) {
								for (var _i = day; _i < end; _i++) {
									//insert guest name to state cells guest property
									c.cells[day]['guest'] = record.name + ' (Check-In)';
									//set condition when there is transpo, t_rate is greater than 0
									// set transpo property to true
									c.cells[day]['transpo'] = record.t_rate > 0 ? true : false;
									//set cell as false which means unavailable/booked/taken
									c.cells[_i]['availability'] = false;
									//set color
									c.cells[_i]['color'] = record.source_color;
									//set as group color
									if (!(end - day == 1) && _i >= day && _i < end - 1) c.cells[_i]['grouped'] = "no-right-border";
								}
							}
						});
					});
					self.setState({ properties: newcells });
				},
				error: function (xhr, status, error) {
					console.log('xhr: ' + xhr.status + ', status: ' + status + ', error: ' + error);
				}
			});
		}
	}, {
		key: 'selectedCell',
		value: function selectedCell(cell, p_id) {
			var p = {
				day: cell,
				month: parseInt(this.state.date.month) + 1,
				year: this.state.date.year,
				property_id: p_id
			};
			var dataString = '\n\t\t\t?day=' + p.day + '&\n\t\t\tmonth=' + p.month + '&\n\t\t\tyear=' + p.year + '&\n\t\t\tproperty_id=' + p.property_id;
			window.location.href = '/reservations/new/' + dataString;
		}
	}, {
		key: 'renderContainer',
		value: function renderContainer() {
			var _this3 = this;

			var days = [];
			for (var i = 1; i <= this.state.date.days; i++) {
				days.push(React.createElement(
					'th',
					{ key: i },
					i
				));
			}
			return React.createElement(
				'div',
				{ className: 'row' },
				React.createElement(
					'div',
					{ className: 'col-md-12' },
					React.createElement(
						'div',
						{ className: 'card' },
						React.createElement(ReservationDate, { res_updateDate: this.updateDate.bind(this) }),
						React.createElement(
							'div',
							{ className: 'card-content table-responsive table-full-width' },
							React.createElement(
								'table',
								{ className: 'reservation' },
								React.createElement(
									'thead',
									{ className: 'reservationHead text-primary' },
									React.createElement(
										'tr',
										null,
										React.createElement(
											'th',
											{ className: 'propertyHeader' },
											'Property'
										),
										days
									)
								),
								React.createElement(
									'tbody',
									null,
									this.state.properties.map(function (item, i) {
										return React.createElement(
											'tr',
											{ key: item.id },
											React.createElement(
												'td',
												null,
												item.name
											),
											item.cells.map(function (cell, i) {
												return React.createElement(ReservationCell, {
													key: cell.id,
													cell: cell,
													property_id: item.id,
													selectCell: _this3.selectedCell });
											})
										);
									})
								)
							)
						)
					)
				)
			);
		}
	}, {
		key: 'render',
		value: function render() {
			return this.renderContainer();
		}
	}]);

	return ReservationApp;
})(React.Component);
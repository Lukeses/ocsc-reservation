var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ReservationDate = (function (_React$Component) {
	_inherits(ReservationDate, _React$Component);

	function ReservationDate(props) {
		_classCallCheck(this, ReservationDate);

		_get(Object.getPrototypeOf(ReservationDate.prototype), "constructor", this).call(this, props);
		//bind
		var d = new Date();
		this.state = {
			month: d.getMonth(),
			year: d.getFullYear()
		};
		this.dateChange = this.dateChange.bind(this);
	}

	_createClass(ReservationDate, [{
		key: "loadOldScript",
		value: function loadOldScript() {
			uc.misc.initFormExtendedDatetimepickers();
		}
	}, {
		key: "componentDidMount",
		value: function componentDidMount() {
			this.loadOldScript();
		}
	}, {
		key: "getMonths",
		value: function getMonths() {
			return ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		}
	}, {
		key: "getYears",
		value: function getYears() {
			var d = new Date();
			//set setback for first year value
			//set forward for last year value
			var setback = 0;
			var forward = 10;
			var years = [];
			var y = d.getFullYear();
			for (var i = y - setback; i <= y + forward; i++) {
				years.push(i);
			}return years;
		}
	}, {
		key: "genListMonths",
		value: function genListMonths(item, i) {
			return React.createElement(
				"option",
				{ key: i,
					value: i },
				item
			);
		}
	}, {
		key: "genListYears",
		value: function genListYears(item, i) {
			return React.createElement(
				"option",
				{ key: i,
					value: item },
				item
			);
		}
	}, {
		key: "dateChange",
		value: function dateChange(e) {
			var _this = this;

			var date = e.target.value;
			var name = e.target.name;
			this.setState(_defineProperty({}, name, date), function () {
				_this.props.res_updateDate(_this.state.month, _this.state.year);
			});
		}
	}, {
		key: "renderDateSelector",
		value: function renderDateSelector() {
			var title = 'Reservation Date';
			var instruction = 'Reservation calendar for room availabitlity.';
			var months = this.getMonths();
			var years = this.getYears();
			return React.createElement(
				"div",
				{ className: "card-header" },
				React.createElement(
					"div",
					{ className: "col-xs-12" },
					React.createElement(
						"h4",
						{ className: "card-title" },
						title
					),
					React.createElement("hr", null),
					React.createElement(
						"div",
						{ className: "row" },
						React.createElement(
							"div",
							{ className: "form-group col-md-2 col-sm-6 col-xs-12" },
							React.createElement(
								"select",
								{ className: "selectpicker",
									"data-style": "btn btn-danger",
									onChange: this.dateChange,
									name: "month",
									ref: "f_month",
									value: this.state.month },
								months.map(this.genListMonths)
							)
						),
						React.createElement(
							"div",
							{ className: "form-group col-md-3 col-sm-6 col-xs-12" },
							React.createElement(
								"select",
								{ className: "selectpicker",
									"data-style": "btn btn-primary",
									onChange: this.dateChange,
									name: "year",
									ref: "f_year",
									value: this.state.year },
								years.map(this.genListYears)
							)
						)
					),
					React.createElement(
						"p",
						{ className: "category" },
						instruction
					)
				)
			);
		}
	}, {
		key: "render",
		value: function render() {
			return this.renderDateSelector();
		}
	}]);

	return ReservationDate;
})(React.Component);
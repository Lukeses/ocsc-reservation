var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ReservationForm = (function (_React$Component) {
  _inherits(ReservationForm, _React$Component);

  function ReservationForm(props) {
    _classCallCheck(this, ReservationForm);

    _get(Object.getPrototypeOf(ReservationForm.prototype), 'constructor', this).call(this, props);
    this.handleChangeDiscount = this.handleChangeDiscount.bind(this);
    this.renderCapacity = this.renderCapacity.bind(this);
    this.append_rental_service = this.append_rental_service.bind(this);
    this.changeProperty = this.changeProperty.bind(this);
    this.existing_client = this.existing_client.bind(this);
    this.new_client = this.new_client.bind(this);
    this.renderClientForm = this.renderClientForm.bind(this);
    this.handleChangeProperty = this.handleChangeProperty.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.state = {
      property: {
        id: 'none',
        name: null,
        rate: 'Select Property',
        capacity: 'Select Property',
        capacity_compare: 0
      },
      no_of_person: '',
      no_of_senior_citizen: '',
      senior_citizen: false,
      checkin: '',
      checkout: '',
      client_mode: 'new',
      client_id: 'none',
      name: '',
      company: '',
      contact: '',
      email: '',
      rental_services: [],
      discount: 'none',
      transportation: 'none',
      source: 'none',
      note: ''

    };
  }

  _createClass(ReservationForm, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var reserve = this.props.reserve;
      this.loadOldScripts();
      //when came from calendar
      //set selected property and date from calendar
      if (reserve.property_id) this.changeProperty(reserve.property_id);
      if (reserve.day && reserve.month && reserve.year) {
        // this.setState({checkin: `${reserve.day}/${reserve.month}/${reserve.year}`})
        this.setState({ checkin: reserve.day + '/' + reserve.month + '/' + reserve.year });
        this.setState({ checkout: reserve.day + '/' + reserve.month + '/' + reserve.year });
      }
    }
  }, {
    key: 'loadOldScripts',
    value: function loadOldScripts() {
      $('.select2').select2();
      uc.misc.initFormExtendedDatetimepickers();
    }
  }, {
    key: 'existing_client',
    value: function existing_client() {
      this.setState({ client_mode: "existing" });
    }
  }, {
    key: 'new_client',
    value: function new_client() {
      this.setState({ client_mode: "new" });
    }
  }, {
    key: 'handleChangeDiscount',
    value: function handleChangeDiscount(id) {
      if (id == "senior_citizen") this.setState({ senior_citizen: true });else this.setState({ senior_citizen: false });
    }
  }, {
    key: 'handleChangeProperty',
    value: function handleChangeProperty(e) {
      var id = e.target.value;
      this.changeProperty(id);
    }
  }, {
    key: 'changeProperty',
    value: function changeProperty(p_id) {
      var id = p_id;
      if (id != "none") {
        var properties = this.props.dropdowns.property;
        var p = properties.filter(function (val) {
          return val.id == id;
        });
        this.setState({
          property: {
            id: p[0].id,
            name: p[0].name,
            rate: p[0].rate.toFixed(2),
            capacity: 'Maximum of ' + p[0].capacity,
            capacity_compare: p[0].capacity
          }
        });
      } else {
        this.setState({
          property: {
            id: 'none',
            name: null,
            rate: 'Select Property',
            capacity: 'Select Property',
            capacity_compare: 0
          }
        });
      }
    }
  }, {
    key: 'handleChange',
    value: function handleChange(e) {
      var _this = this;

      var value = e.target.value;
      var name = e.target.name;
      this.setState(_defineProperty({}, name, value), function () {
        //special case for senior citizen discount only
        if (name == "discount") _this.handleChangeDiscount(value);
        // console.log(this.state[name])
      });
    }
  }, {
    key: 'handleSave',
    value: function handleSave() {
      var _this2 = this;

      //rental_services is not in state due to
      //select2 issues note detecting dom event
      //use jquery manually instead to get and insert value
      self = this;
      this.setState({ rental_services: $('#rental_services').val() }, function () {
        //start saving
        if (_this2.validateReservationForm(_this2.state)) {
          // toastr.info(`Please Wait`,`Saving`)
          _this2.refs.btn_save.disabled = true;
          $.ajax({
            url: '/reservations',
            data: self.state,
            type: "POST",
            success: function (s) {
              switch (s.status) {
                case "success":
                  toastr.success('New Reservation', 'Saved');
                  swal({
                    title: "Saved",
                    text: "New Reservation has been saved",
                    type: "success",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                  }, function () {
                    window.setTimeout(function () {
                      location.href = '/reservations';
                    }, 1);
                  });
                  window.setTimeout(function () {
                    location.href = '/reservations';
                  }, 2000);
                  break;

                case "duplicate":
                  toastr.warning(s.message, s.status.toUpperCase());
                  _this2.refs.btn_save.disabled = false;
                  break;

                case "error":
                  var _loop = function (err) {
                    s.error[err].map(function (message) {
                      toastr.warning(message, err.toUpperCase());
                    });
                    _this2.refs.btn_save.disabled = false;
                  };

                  for (var err in s.error) {
                    _loop(err);
                  }
                  break;
              }
            },
            error: function (xhr, status, error) {
              toastr.error('Something Happended', 'Error');
              console.log('xhr: ' + xhr.status + ', status: ' + status + ', error: ' + error);
              _this2.refs.btn_save.disabled = false;
            }
          });
        } else {
          toastr.error('Please Fill Out Required Fields. Labels with *', 'Incomplete Fields');
        }
      });
    }
  }, {
    key: 'validateReservationForm',
    value: function validateReservationForm(f) {
      var v = true;

      //specific validation adding has-error to div wrapper of element
      //property
      if (f.property.id == 'none') {
        $('#d_property').addClass('has-error');
        v = false;
      } else {
        $('#d_property').removeClass('has-error');
      }

      //source
      if (f.source == 'none') {
        $('#d_source').addClass('has-error');
        toastr.warning('Must not be Empty', 'Source');
        v = false;
      } else {
        $('#d_source').removeClass('has-error');
      }

      //checkin
      if (f.checkin == '') {
        $('#d_checkin').addClass('has-error');
        v = false;
      } else {
        $('#d_checkin').removeClass('has-error');
      }

      //checkout
      if (f.checkout == '') {
        $('#d_checkout').addClass('has-error');
        v = false;
      } else {
        $('#d_checkout').removeClass('has-error');
      }

      // if checkin and checkout are the same
      if (f.checkout == f.checkin) {
        $('#d_checkout').addClass('has-error');
        $('#d_checkin').addClass('has-error');
        toastr.warning('Check-In and Check-Out dates cannot be the same', 'Reservation Dates');
        v = false;
      } else {
        $('#d_checkin').removeClass('has-error');
        $('#d_checkout').removeClass('has-error');
      }

      if (f.client_mode == "new") {
        //name
        if (f.name == '') {
          $('#d_name').addClass('has-error');
          v = false;
        } else {
          $('#d_name').removeClass('has-error');
        }
      } else if (f.client_mode == "existing") {
        //client (registered guets)
        if (f.client_id == 'none') {
          $('#d_client').addClass('has-error');
          v = false;
        } else {
          $('#d_client').removeClass('has-error');
        }
      }

      //capacity validation
      //if senior citizen is selected
      if (f.senior_citizen === false) {
        //no_of_person
        if (f.no_of_person == '') {
          $('#d_capacity').addClass('has-error');
          v = false;
        }
        //if negative or 0
        else {
            if (f.no_of_person <= 0) {
              $('#d_capacity').addClass('has-error');
              toastr.warning('Invalid Value', 'No. of Person');
              v = false;
            } else if (f.no_of_person > f.property.capacity_compare) {
              $('#d_capacity').addClass('has-error');
              toastr.warning('Exceeded Property Capacity', 'No. of Person');
              v = false;
            } else {
              $('#d_capacity').removeClass('has-error');
            }
          }
      } else {
        //no_of_guest
        var no_of_guest = 0;
        no_of_guest = parseInt(f.no_of_person) + parseInt(f.no_of_senior_citizen);
        console.log(no_of_guest);
        if (f.no_of_person == '' && f.no_of_senior_citizen == '') {
          $('#d_capacity').addClass('has-error');
          v = false;
        }
        //if negative or 0
        else {
            if (no_of_guest <= 0) {
              $('#d_capacity').addClass('has-error');
              toastr.warning('Invalid Value', 'No. Guests');
              v = false;
            } else if (no_of_guest > f.property.capacity_compare) {
              $('#d_capacity').addClass('has-error');
              toastr.warning('Exceeded Property Capacity', 'No. of Guests');
              v = false;
            } else {
              $('#d_capacity').removeClass('has-error');
            }
          }
      }

      return v;
    }
  }, {
    key: 'append_rental_service',
    value: function append_rental_service(id, text) {
      $('#rental_services').select2({
        data: [{ id: id, text: text, selected: true }]
      });
    }
  }, {
    key: 'new_rental_service',
    value: function new_rental_service() {
      $('#modal_rental_services').modal("show");
    }
  }, {
    key: 'renderCapacity',
    value: function renderCapacity() {
      if (this.state.senior_citizen === true) {
        return React.createElement(
          'div',
          null,
          React.createElement(
            'div',
            { className: 'form-group col-sm-4 col-xs-12', id: 'd_capacity' },
            React.createElement(
              'label',
              { className: 'control-label' },
              'Capacity'
            ),
            React.createElement('input', { className: 'form-control', value: this.state.property.capacity, type: 'text', disabled: true })
          ),
          React.createElement(
            'div',
            { className: 'form-group col-sm-4 col-xs-12', id: 'd_no_of_person' },
            React.createElement(
              'label',
              { className: 'control-label' },
              'No. of Person(s) ',
              React.createElement(
                'star',
                null,
                '*'
              )
            ),
            React.createElement('input', { className: 'form-control',
              onChange: this.handleChange,
              name: 'no_of_person',
              type: 'number',
              required: 'true' })
          ),
          React.createElement(
            'div',
            { className: 'form-group col-sm-4 col-xs-12', id: 'd_no_of_senior_citizen' },
            React.createElement(
              'label',
              { className: 'control-label' },
              'No. of Senior Citizen(s) ',
              React.createElement(
                'star',
                null,
                '*'
              )
            ),
            React.createElement('input', { className: 'form-control',
              onChange: this.handleChange,
              name: 'no_of_senior_citizen',
              type: 'number',
              required: 'true' })
          )
        );
      } else {
        return React.createElement(
          'div',
          null,
          React.createElement(
            'div',
            { className: 'form-group col-sm-8 col-xs-12', id: 'd_capacity' },
            React.createElement(
              'label',
              { className: 'control-label' },
              'Capacity'
            ),
            React.createElement('input', { className: 'form-control', value: this.state.property.capacity, type: 'text', disabled: true })
          ),
          React.createElement(
            'div',
            { className: 'form-group col-sm-4 col-xs-12', id: 'd_no_of_person' },
            React.createElement(
              'label',
              { className: 'control-label' },
              'No. of Person(s) ',
              React.createElement(
                'star',
                null,
                '*'
              )
            ),
            React.createElement('input', { className: 'form-control',
              onChange: this.handleChange,
              name: 'no_of_person',
              type: 'number',
              required: 'true' })
          )
        );
      }
    }
  }, {
    key: 'renderForm',
    value: function renderForm() {
      return React.createElement(
        'div',
        null,
        React.createElement(QuickRentalServices, { append_rental_service: this.append_rental_service }),
        React.createElement(
          'form',
          { ref: 'form_reservations_new' },
          React.createElement(
            'div',
            { className: 'card-content' },
            React.createElement(
              'div',
              { className: 'row' },
              React.createElement(
                'div',
                { className: 'col-sm-6' },
                React.createElement(
                  'div',
                  { className: 'row' },
                  React.createElement(
                    'div',
                    { className: 'form-group col-sm-8 col-xs-12', id: 'd_property' },
                    React.createElement(
                      'label',
                      { className: 'control-label' },
                      'Property ',
                      React.createElement(
                        'star',
                        null,
                        '*'
                      )
                    ),
                    React.createElement(
                      'select',
                      { ref: 'f_property',
                        className: 'form-control',
                        type: 'text',
                        value: this.state.property.id,
                        onChange: this.handleChangeProperty,
                        disabled: 'true',
                        required: 'true' },
                      React.createElement(
                        'option',
                        { value: 'none' },
                        'Select Property'
                      ),
                      this.props.dropdowns.property.map(function (p, i) {
                        return React.createElement(
                          'option',
                          { key: p.id, value: p.id },
                          p.name
                        );
                      })
                    )
                  ),
                  React.createElement(
                    'div',
                    { className: 'form-group col-sm-4 col-xs-12' },
                    React.createElement(
                      'label',
                      { className: 'control-label' },
                      'Per Night Rate'
                    ),
                    React.createElement('input', { className: 'form-control',
                      value: this.state.property.rate,
                      type: 'text', disabled: true })
                  ),
                  this.renderCapacity(),
                  React.createElement(
                    'div',
                    { className: 'form-group col-xs-6', id: 'd_checkin' },
                    React.createElement(
                      'label',
                      { className: 'control-label' },
                      'Check-in Date',
                      React.createElement(
                        'star',
                        null,
                        '*'
                      )
                    ),
                    React.createElement('input', { className: 'form-control datetimepicker',
                      name: 'checkin',
                      onBlur: this.handleChange,
                      value: this.state.checkin,
                      disabled: 'true',
                      required: 'true' })
                  ),
                  React.createElement(
                    'div',
                    { className: 'form-group col-xs-6', id: 'd_checkout' },
                    React.createElement(
                      'label',
                      { className: 'control-label' },
                      'Check-out Date',
                      React.createElement(
                        'star',
                        null,
                        '*'
                      )
                    ),
                    React.createElement('input', { className: 'form-control datetimepicker',
                      name: 'checkout',
                      onBlur: this.handleChange,
                      value: this.state.checkout,
                      rel: 'tooltip',
                      title: 'Day/Month/Year',
                      required: 'true' })
                  ),
                  React.createElement(
                    'div',
                    { className: 'form-group col-xs-9' },
                    React.createElement(
                      'label',
                      { className: 'control-label' },
                      'Rentals and Services'
                    ),
                    React.createElement(
                      'select',
                      { ref: 'rental_service',
                        className: 'form-control select2',
                        id: 'rental_services',
                        name: 'rental_services',
                        multiple: 'multiple' },
                      this.props.dropdowns.rental_service.map(function (s, i) {
                        return React.createElement(
                          'option',
                          { key: s.id, value: s.id },
                          s.description,
                          ' (',
                          parseFloat(s.rate),
                          ')'
                        );
                      })
                    )
                  ),
                  React.createElement(
                    'div',
                    { className: 'form-group col-xs-2 pull-left' },
                    React.createElement(
                      'label',
                      { className: 'invisible' },
                      'Add'
                    ),
                    React.createElement(
                      'button',
                      { className: 'btn btn-simple btn-magnify',
                        onClick: this.new_rental_service,
                        type: 'button',
                        title: 'Add Rental Service' },
                      React.createElement('i', { className: 'ti-plus' })
                    )
                  )
                )
              ),
              React.createElement('div', { className: 'col-sm-1' }),
              React.createElement(
                'div',
                { className: 'col-sm-6' },
                React.createElement(
                  'div',
                  { className: 'row' },
                  this.renderClientForm(),
                  React.createElement(
                    'div',
                    { className: 'form-group col-xs-12' },
                    React.createElement(
                      'label',
                      { className: 'control-label' },
                      'Note'
                    ),
                    React.createElement('textarea', { name: 'note',
                      onChange: this.handleChange,
                      className: 'form-control' })
                  )
                )
              )
            ),
            React.createElement('hr', null),
            React.createElement(
              'div',
              { className: 'row' },
              React.createElement(
                'div',
                { className: 'form-group col-sm-4 col-xs-12' },
                React.createElement(
                  'label',
                  { className: 'control-label' },
                  'Discounts'
                ),
                React.createElement(
                  'select',
                  { ref: 'f_discount',
                    className: 'selectpicker',
                    'data-style': 'btn btn-success btn-block',
                    title: 'Select Discount',
                    name: 'discount',
                    onChange: this.handleChange,
                    'data-size': '7' },
                  React.createElement(
                    'option',
                    { value: 'none' },
                    'No Discount'
                  ),
                  this.props.dropdowns.discount.map(function (d, i) {
                    return React.createElement(
                      'option',
                      { key: d.id, value: d.id },
                      d.name,
                      ' ',
                      d.amount,
                      ' %'
                    );
                  })
                )
              ),
              React.createElement(
                'div',
                { className: 'form-group col-sm-4 col-xs-12', id: 'd_transportation' },
                React.createElement(
                  'label',
                  { className: 'control-label' },
                  'Transportation '
                ),
                React.createElement(
                  'select',
                  { ref: 'f_transportation',
                    className: 'selectpicker',
                    'data-style': 'btn btn-danger btn-block',
                    title: 'Select Transpo Service',
                    name: 'transportation',
                    onChange: this.handleChange,
                    'data-size': '7' },
                  React.createElement(
                    'option',
                    { value: 'none' },
                    'No Transpo Service (0.00)'
                  ),
                  this.props.dropdowns.transportation.map(function (t, i) {
                    return React.createElement(
                      'option',
                      { key: t.id, value: t.id },
                      t.description,
                      ' (',
                      t.rate,
                      ')'
                    );
                  })
                )
              ),
              React.createElement(
                'div',
                { className: 'form-group col-sm-4 col-xs-12', id: 'd_source' },
                React.createElement(
                  'label',
                  { className: 'control-label' },
                  'Source ',
                  React.createElement(
                    'star',
                    null,
                    '*'
                  )
                ),
                React.createElement(
                  'select',
                  { ref: 'f_source',
                    className: 'selectpicker',
                    'data-style': 'btn btn-warning btn-block',
                    title: 'Select Source',
                    name: 'source',
                    onChange: this.handleChange,
                    'data-size': '7' },
                  React.createElement(
                    'option',
                    { value: 'none' },
                    'Select Source'
                  ),
                  this.props.dropdowns.sources.map(function (s, i) {
                    return React.createElement(
                      'option',
                      { key: s.id, value: s.id },
                      s.name
                    );
                  })
                )
              )
            ),
            React.createElement(
              'div',
              { className: 'category' },
              React.createElement(
                'star',
                null,
                '*'
              ),
              'Required fields'
            )
          ),
          React.createElement(
            'div',
            { className: 'card-footer text-center' },
            React.createElement(
              'button',
              { type: 'button',
                onClick: this.handleSave,
                ref: 'btn_save',
                className: 'btn_save btn btn-lg btn-info btn-fill btn-wd' },
              'Save Reservation'
            )
          )
        )
      );
    }
  }, {
    key: 'renderClientForm',
    value: function renderClientForm() {
      if (this.state.client_mode == "new") return this.renderNewClient();else return this.renderExistingClient();
    }
  }, {
    key: 'renderExistingClient',
    value: function renderExistingClient() {
      return React.createElement(
        'div',
        null,
        React.createElement(
          'div',
          { className: 'form-group col-xs-1' },
          React.createElement(
            'label',
            { className: 'invisible' },
            'Search'
          ),
          React.createElement(
            'button',
            { className: 'btn btn-icon btn-magnify',
              onClick: this.new_client,
              type: 'button',
              title: 'Add New Guest' },
            React.createElement('i', { className: 'ti-plus' })
          )
        ),
        React.createElement(
          'div',
          { className: 'form-group col-sm-11 col-xs-12', id: 'd_client' },
          React.createElement(
            'label',
            { className: 'control-label' },
            'Registered Guest',
            React.createElement(
              'star',
              null,
              '*'
            )
          ),
          React.createElement(
            'select',
            { className: 'form-control',
              name: 'client_id',
              onChange: this.handleChange,
              required: 'true' },
            React.createElement(
              'option',
              { value: 'none' },
              'Select Guest'
            ),
            this.props.dropdowns.clients.map(function (client) {
              return React.createElement(
                'option',
                { value: client.id, key: client.id },
                client.name
              );
            })
          )
        )
      );
    }
  }, {
    key: 'renderNewClient',
    value: function renderNewClient() {
      return React.createElement(
        'div',
        null,
        React.createElement(
          'div',
          { className: 'form-group col-xs-1' },
          React.createElement(
            'label',
            { className: 'invisible' },
            'Search'
          ),
          React.createElement(
            'button',
            { className: 'btn btn-icon btn-magnify',
              onClick: this.existing_client,
              type: 'button',
              title: 'Search Registered Guest' },
            React.createElement('i', { className: 'ti-search' })
          )
        ),
        React.createElement(
          'div',
          { className: 'form-group col-sm-5 col-xs-12', id: 'd_name' },
          React.createElement(
            'label',
            { className: 'control-label' },
            'Guests Full Name',
            React.createElement(
              'star',
              null,
              '*'
            )
          ),
          React.createElement('input', { className: 'form-control',
            onChange: this.handleChange,
            name: 'name',
            type: 'text',
            required: 'true' })
        ),
        React.createElement(
          'div',
          { className: 'form-group col-sm-6 col-xs-12' },
          React.createElement(
            'label',
            { className: 'control-label' },
            'Company Name'
          ),
          React.createElement('input', { className: 'form-control', onChange: this.handleChange, name: 'company', type: 'text' })
        ),
        React.createElement(
          'div',
          { className: 'form-group col-sm-6 col-xs-12' },
          React.createElement(
            'label',
            { className: 'control-label' },
            'Email Address'
          ),
          React.createElement('input', { className: 'form-control', onChange: this.handleChange, name: 'email', type: 'email' })
        ),
        React.createElement(
          'div',
          { className: 'form-group col-sm-6 col-xs-12' },
          React.createElement(
            'label',
            { className: 'control-label' },
            'Contact Number'
          ),
          React.createElement('input', { className: 'form-control', name: 'contact', onChange: this.handleChange, type: 'number' })
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var dropdowns = this.props.dropdowns;
      return React.createElement(
        'div',
        null,
        this.renderForm()
      );
    }
  }]);

  return ReservationForm;
})(React.Component);
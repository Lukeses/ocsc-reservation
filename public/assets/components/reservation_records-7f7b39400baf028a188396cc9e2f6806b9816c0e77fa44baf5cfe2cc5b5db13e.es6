var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ReservationRecords = (function (_React$Component) {
  _inherits(ReservationRecords, _React$Component);

  function ReservationRecords(props) {
    _classCallCheck(this, ReservationRecords);

    _get(Object.getPrototypeOf(ReservationRecords.prototype), "constructor", this).call(this, props);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleUnfilter = this.handleUnfilter.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.filter = this.filter.bind(this);
    this.getAjaxRecords = this.getAjaxRecords.bind(this);
    this.recordCheckIn = this.recordCheckIn.bind(this);
    this.recordCheckOut = this.recordCheckOut.bind(this);
    this.receipt = this.receipt.bind(this);
    this.renderReceipt = this.renderReceipt.bind(this);
    this.renderMain = this.renderMain.bind(this);
    this.handlePayment = this.handlePayment.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.modal = {
      id: "modal_view",
      size: "modal-md",
      icon: "",
      title: "Reservation Information",
      filter: {
        id: "modal_filter",
        size: "modal-xs",
        icon: "",
        title: "Filter"
      }
    };
    this.state = {
      records: [],
      receipt: {}
    };
  }

  _createClass(ReservationRecords, [{
    key: "loadOldScripts",
    value: function loadOldScripts() {
      //$(`#dt_reservation_records`).dataTable()
      uc.sidebar.menu_name1("reservations");
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.loadOldScripts();
      this.getAjaxRecords();
    }
  }, {
    key: "recordCheckIn",
    value: function recordCheckIn(soli) {
      var _this = this;

      var palit = this.state.records;
      palit[soli.index].status2 = 2;
      this.setState({
        records: palit
      }, function () {
        _this.updateReservation(palit[soli.index].id, 'checkin');
      });
    }
  }, {
    key: "recordCheckOut",
    value: function recordCheckOut(soli) {
      var _this2 = this;

      var palit = this.state.records;
      palit[soli.index].status2 = 3;
      this.setState({
        records: palit
      }, function () {
        _this2.updateReservation(palit[soli.index].id, 'checkout');
      });
    }
  }, {
    key: "updateReservation",
    value: function updateReservation(id, type) {
      self = this;
      $.ajax({
        url: "/reservations/" + type,
        data: { id: id },
        type: "POST",
        success: function (s) {
          self.getAjaxRecords();
        },
        error: function (xhr, status, error) {
          console.log("xhr: " + xhr.status + ", status: " + status + ", error: " + error);
        }
      });
    }
  }, {
    key: "getAjaxRecords",
    value: function getAjaxRecords() {
      self = this;
      $.ajax({
        url: "/reservations/records",
        type: "POST",
        success: function (s) {
          self.setState({ records: s });
        },
        error: function (xhr, status, error) {
          console.log("xhr: " + xhr.status + ", status: " + status + ", error: " + error);
        }
      });
    }
  }, {
    key: "handleSearch",
    value: function handleSearch(s) {
      this.setState({ records: s });
    }
  }, {
    key: "handleCancel",
    value: function handleCancel() {
      self = this;
      //confirmation
      swal({
        title: 'Are you sure?',
        text: "Cancel this Reservation",
        type: 'warning',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirm Cancellation'
      }).then(function () {
        $.ajax({
          url: "/reservations/cancel",
          data: { id: self.state.receipt.id },
          type: "POST",
          success: function (s) {
            switch (s.status) {
              case "success":
                self.setState({ receipt: {} }, function () {
                  $("#" + self.modal.id).modal('hide');
                  self.getAjaxRecords();
                  toastr.info("Reservation\n                  has been Cancelled", "Cancellation  Done");
                });
                break;
            }
          },
          error: function (xhr, status, error) {
            console.log("xhr: " + xhr.status + ", status: " + status + ", error: " + error);
          }
        });
      });
    }
  }, {
    key: "clearPayment",
    value: function clearPayment() {
      this.refs.amount.value = "";
    }
  }, {
    key: "handlePayment",
    value: function handlePayment() {
      self = this;
      var r = this.state.receipt;
      var total = parseFloat(r.total - r.d_rate - r.total_payments);
      var amount = parseFloat(this.refs.amount.value);
      if (amount) {
        if (amount <= total) {
          this.refs.btn_payment.disabled = true;
          //confirmation
          swal({
            title: 'Make Payment?',
            text: "Pay with this amount?",
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
          }).then(function () {
            $.ajax({
              url: "/payments",
              type: "POST",
              data: { amount: amount, booking_id: r.id },
              success: function (s) {
                switch (s.status) {
                  case 'success':
                    self.refs.btn_payment.disabled = false;
                    self.clearPayment();
                    self.getAjaxRecords();
                    $("#" + self.modal.id).modal('hide');
                    swal('Payment Made', '', 'success');
                    break;
                  case 'error':
                    var _loop = function (err) {
                      s.error[err].map(function (message) {
                        toastr.warning(message, err.toUpperCase());
                      });
                      self.refs.btn_payment.disabled = false;
                    };

                    for (var err in s.error) {
                      _loop(err);
                    }
                    break;
                }
              },
              error: function (xhr, status, error) {
                console.log("xhr: " + xhr.status + ", status: " + status + ", error: " + error);
                self.refs.btn_payment.disabled = false;
              }

            });
          });
          this.refs.btn_payment.disabled = false;
        } else {
          swal("Exceeded Amount Due", "", "warning");
        }
      } else {
        toastr.info('Enter an Amount', 'Payment Field is Blank');
      }
    }
  }, {
    key: "receipt",
    value: function receipt(record) {
      var _this3 = this;

      //clears first
      this.setState({ receipt: {} }, function () {
        //pre load receipt state
        _this3.setState({ receipt: record }, function () {
          _this3.clearPayment();
          $("#" + _this3.modal.id).modal('show');
        });
      });
    }
  }, {
    key: "renderReceipt",
    value: function renderReceipt() {
      var r = this.state.receipt;
      var amount_due = r.total - r.d_rate - r.payment;
      var amount_due_class = amount_due == 0 ? "hidden" : "";
      var t = uc.transform;
      return React.createElement(
        "div",
        null,
        React.createElement(
          "table",
          { className: "table" },
          React.createElement(
            "tbody",
            null,
            React.createElement(
              "tr",
              null,
              React.createElement(
                "th",
                null,
                "Property Name"
              ),
              React.createElement(
                "td",
                null,
                r.property_name
              )
            ),
            React.createElement(
              "tr",
              null,
              React.createElement(
                "th",
                null,
                "Guest"
              ),
              React.createElement(
                "td",
                null,
                r.name
              )
            ),
            React.createElement(
              "tr",
              null,
              React.createElement(
                "th",
                null,
                "Check-In"
              ),
              React.createElement(
                "td",
                null,
                t.format_d_m_y(null, r.checkin)
              )
            ),
            React.createElement(
              "tr",
              null,
              React.createElement(
                "th",
                null,
                "Check-Out"
              ),
              React.createElement(
                "td",
                null,
                t.format_d_m_y(null, r.checkout)
              )
            ),
            React.createElement(
              "tr",
              null,
              React.createElement(
                "th",
                null,
                "Rate:"
              ),
              React.createElement(
                "td",
                null,
                t.peso(null, r.p_rate / r.nights)
              )
            ),
            React.createElement(
              "tr",
              null,
              React.createElement(
                "th",
                null,
                "No Of Nights: "
              ),
              React.createElement(
                "td",
                null,
                r.nights
              )
            ),
            React.createElement(
              "tr",
              null,
              React.createElement(
                "th",
                null,
                "Subtotal:"
              ),
              React.createElement(
                "td",
                null,
                t.peso(null, r.p_rate)
              )
            ),
            React.createElement(
              "tr",
              null,
              React.createElement(
                "th",
                null,
                "Transporation Charge:"
              ),
              React.createElement(
                "td",
                null,
                t.peso(null, r.t_rate)
              )
            ),
            React.createElement(
              "tr",
              null,
              React.createElement(
                "th",
                null,
                "Rentals and Services:"
              ),
              React.createElement(
                "td",
                null,
                t.peso(null, r.rs_rate)
              )
            ),
            React.createElement(
              "tr",
              { className: "info" },
              React.createElement(
                "th",
                null,
                "Discounts and Promos"
              ),
              React.createElement(
                "td",
                null,
                "(LESS " + t.peso(null, r.d_rate) + " )"
              )
            ),
            React.createElement(
              "tr",
              { className: "warning" },
              React.createElement(
                "th",
                null,
                "Total"
              ),
              React.createElement(
                "td",
                null,
                t.peso(null, r.total)
              )
            ),
            React.createElement(
              "tr",
              { className: "success" },
              React.createElement(
                "th",
                null,
                "Paid Amount"
              ),
              React.createElement(
                "td",
                null,
                t.peso(null, r.payment)
              )
            ),
            React.createElement(
              "tr",
              { className: "danger" },
              React.createElement(
                "th",
                null,
                "Total Amount Due"
              ),
              React.createElement(
                "td",
                null,
                t.peso(null, amount_due)
              )
            )
          )
        ),
        React.createElement(
          "div",
          { className: "input-group " + amount_due_class },
          React.createElement("input", { className: "form-control",
            ref: "amount",
            placeholder: "Enter Payment Amount",
            type: "number" }),
          React.createElement(
            "span",
            { className: "input-group-btn" },
            React.createElement(
              "button",
              {
                ref: "btn_payment",
                onClick: this.handlePayment,
                className: "btn btn-success btn-fill btn-magnify " + amount_due_class },
              React.createElement("i", { className: "ti-plus" }),
              " Make Payment"
            )
          )
        ),
        React.createElement(
          "div",
          { className: "card-footer text-center" },
          React.createElement(
            "div",
            { className: "btn-group" },
            React.createElement(
              "a",
              {
                href: "/reservations/voucher?booking_id=" + r.id,
                target: "_new",
                type: "button",
                ref: "btn_voucher",
                onClick: this.handleVoucher,
                className: "btn btn-primary btn-fill btn-wd" },
              React.createElement("i", { className: "ti-printer" }),
              " Print"
            ),
            React.createElement(
              "a",
              {
                href: "/reservations/voucher?booking_id=" + r.id,
                target: "_new",
                type: "button",
                ref: "btn_mailer",
                onClick: this.handleMailer,
                className: "btn btn-primary btn-fill btn-wd" },
              React.createElement("i", { className: "ti-email" }),
              " Mail"
            ),
            React.createElement(
              "button",
              {
                type: "button",
                onClick: this.handleCancel,
                className: "btn btn-danger btn-fill btn-wd" },
              React.createElement("i", { className: "ti-close" }),
              " Cancel Booking"
            )
          )
        )
      );
    }
  }, {
    key: "handleUnfilter",
    value: function handleUnfilter() {
      this.getAjaxRecords();
      this.refs.filter_checkin.value = "";
      $("#" + this.modal.filter.id).modal('hide');
    }
  }, {
    key: "handleFilter",
    value: function handleFilter() {
      self = this;
      var data = {
        checkin: this.refs.filter_checkin.value
      };

      $.ajax({
        url: "/reservations/filter",
        data: data,
        type: "POST",
        success: function (s) {
          switch (s.status) {
            case "success":
              self.setState({ records: s.records }, function () {
                $("#" + self.modal.filter.id).modal('hide');
              });
              break;
          }
        },
        error: function (xhr, status, error) {
          toastr.error("Something Happended", "Error");
          console.log("xhr: " + xhr.status + ", status: " + status + ", error: " + error);
        }
      });
    }
  }, {
    key: "renderFilter",
    value: function renderFilter() {
      return React.createElement(
        "div",
        null,
        React.createElement(
          "div",
          { className: "row text-center" },
          React.createElement(
            "div",
            { className: "col-sm-8 col-sm-offset-2" },
            React.createElement(
              "label",
              {
                className: "control-label text-left" },
              "Check-In Date"
            ),
            React.createElement("input", {
              ref: "filter_checkin",
              className: "form-control",
              type: "date",
              name: "filter_checkin" }),
            React.createElement("hr", null),
            React.createElement(
              "div",
              { className: "btn-group" },
              React.createElement(
                "button",
                {
                  onClick: this.handleUnfilter,
                  className: "btn btn-warning btn-fill btn-wd" },
                React.createElement("i", { className: "ti-close" }),
                " Remove Filter"
              ),
              React.createElement(
                "button",
                {
                  onClick: this.handleFilter,
                  className: "btn btn-info btn-fill btn-wd" },
                React.createElement("i", { className: "ti-filter" }),
                " Filter"
              )
            )
          )
        )
      );
    }
  }, {
    key: "filter",
    value: function filter() {
      $("#" + this.modal.filter.id).modal('show');
    }
  }, {
    key: "renderMain",
    value: function renderMain() {
      var thead = ['Guest', 'Transpo', 'CheckIn', 'CheckOut', 'Property', 'Source', 'Balance', 'Payment', 'Reservation', 'Status'];
      var tdata = [{ column: 'name' }, { column: 'transportation_name' }, { column: 'checkin', transform: 'format_d_m_y' }, { column: 'checkout', transform: 'format_d_m_y' }, { column: 'property_name' }, { column: 'source_name' }, { column: 'balance', transform: 'peso' }, { column: 'payment', transform: 'peso' }, { column: 'status2', transform: 'status_reservation' }, { column: 'status3', transform: 'status_payment' }];
      return React.createElement(
        "div",
        null,
        React.createElement(
          Modal,
          { modal: this.modal },
          this.renderReceipt()
        ),
        React.createElement(
          Modal,
          { modal: this.modal.filter },
          this.renderFilter()
        ),
        React.createElement(
          Container,
          null,
          React.createElement(
            TableSearch,
            {
              searched: this.handleSearch,
              url: "reservations/search" },
            React.createElement(Filter, { filter: this.filter })
          ),
          React.createElement(Table, {
            data: this.state.records,
            thead: thead,
            tdata: tdata,
            taction: "reservation_records",
            checkin: this.recordCheckIn,
            checkout: this.recordCheckOut,
            receipt: this.receipt,
            classes: "table-hover table-striped"
          })
        )
      );
    }
  }, {
    key: "render",
    value: function render() {
      return React.createElement(
        "div",
        null,
        this.renderMain()
      );
    }
  }]);

  return ReservationRecords;
})(React.Component);
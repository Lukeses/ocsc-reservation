var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ReservationRecordsRow = (function (_React$Component) {
  _inherits(ReservationRecordsRow, _React$Component);

  function ReservationRecordsRow(props) {
    _classCallCheck(this, ReservationRecordsRow);

    _get(Object.getPrototypeOf(ReservationRecordsRow.prototype), "constructor", this).call(this, props);
    this.Checkin = this.Checkin.bind(this);
    this.Checkout = this.Checkout.bind(this);
    this.View = this.View.bind(this);
  }

  _createClass(ReservationRecordsRow, [{
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "setCellColor",
    value: function setCellColor(s, type) {
      if (type == "reservation") {
        if (s == 1) {
          return "warning";
        } else if (s == 2) {
          return "info";
        } else if (s == 3) {
          return "success";
        }
      } else if (type == "payment") {
        if (s == 1) {
          return "danger";
        } else if (s == 2) {
          return "success";
        }
      }
    }
  }, {
    key: "setStatusName",
    value: function setStatusName(s, type) {
      if (type == "reservation") {
        if (s == 1) {
          return "Reserved";
        } else if (s == 2) {
          return "Checked-In";
        } else if (s == 3) {
          return "Checked-Out";
        }
      } else if (type == "payment") {
        if (s == 1) {
          return "Amount Due";
        } else if (s == 2) {
          return "Fully Paid";
        }
      }
    }
  }, {
    key: "setDisability",
    value: function setDisability(s, type) {
      if (type == "checkin") {
        if (s == 1) {
          return false;
        } else {
          return true;
        }
      }
      if (type == "checkout") {
        if (s == 2) {
          return false;
        } else {
          return true;
        }
      }
    }
  }, {
    key: "formatDate",
    value: function formatDate(date) {
      return date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear() + " ";
    }
  }, {
    key: "Checkin",
    value: function Checkin() {
      self = this;
      swal({
        title: 'Confirm Check In',
        text: "",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Check In'
      }).then(function () {
        self.props.checkin({ id: self.props.record.id, index: self.props.index });
        swal('Checked In!', 'Reservation has been Checked In', 'success');
        toastr.success('Checked In');
      });
    }
  }, {
    key: "Checkout",
    value: function Checkout() {
      self = this;
      swal({
        title: 'Confirm Check Out',
        text: "",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Check Out'
      }).then(function () {
        self.props.checkout({ id: self.props.record.id, index: self.props.index });
        swal('Checked Out!', 'Successfully set Checked Out', 'success');
        toastr.success('Checked Out');
      });
    }
  }, {
    key: "View",
    value: function View() {
      self = this;
      var pass = this.props.record;
      $.ajax({
        url: "/payments/total_payment",
        type: "POST",
        data: { booking_id: this.props.record.id },
        success: function (s) {
          pass.total_payments = s;
          self.props.receipt(pass);
        },
        error: function (xhr, status, error) {
          console.log("xhr: " + xhr.status + ", status: " + status + ", error: " + error);
        }
      });
    }
  }, {
    key: "renderRow",
    value: function renderRow() {
      var r = this.props.record;
      return React.createElement(
        "div",
        { className: "table-icons" },
        React.createElement(
          "button",
          { rel: "tooltip",
            className: "btn btn-simple btn-info btn-icon table-action view",
            ref: "action_checkin",
            disabled: this.setDisability(r.status2, "checkin"),
            onClick: this.Checkin,
            "data-original-title": "Check-in" },
          React.createElement("i", { className: "ti-tag" })
        ),
        React.createElement(
          "button",
          { rel: "tooltip",
            className: "btn btn-simple btn-warning btn-icon table-action edit",
            disabled: this.setDisability(r.status2, "checkout"),
            onClick: this.Checkout,
            "data-original-title": "Check-out" },
          React.createElement("i", { className: "ti-bell" })
        ),
        React.createElement(
          "button",
          { rel: "tooltip",
            className: "btn btn-simple btn-danger btn-icon table-action remove",
            onClick: this.View,
            "data-original-title": "View Receipt" },
          React.createElement("i", { className: "ti-stamp" })
        )
      );
    }
  }, {
    key: "render",
    value: function render() {
      return this.renderRow();
    }
  }]);

  return ReservationRecordsRow;
})(React.Component);
var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Sources = (function (_React$Component) {
	_inherits(Sources, _React$Component);

	function Sources(props) {
		_classCallCheck(this, Sources);

		_get(Object.getPrototypeOf(Sources.prototype), "constructor", this).call(this, props);
		this.renderMain = this.renderMain.bind(this);
		this.handleSearch = this.handleSearch.bind(this);
		this.getAjaxRecords = this.getAjaxRecords.bind(this);
		this["new"] = this["new"].bind(this);
		this["delete"] = this["delete"].bind(this);
		this.fetch = this.fetch.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.save = this.save.bind(this);
		this.update = this.update.bind(this);
		this.modal = {
			id: "modal_sources",
			size: "modal-md",
			icon: "",
			title: "Source"
		};
		this.state = {
			records: [],
			mode: null,
			name: '',
			color: ''
		};
	}

	_createClass(Sources, [{
		key: "loadOldScripts",
		value: function loadOldScripts() {
			uc.sidebar.menu_name1("sources");
		}
	}, {
		key: "componentDidMount",
		value: function componentDidMount() {
			this.loadOldScripts();
			this.getAjaxRecords();
		}
	}, {
		key: "getAjaxRecords",
		value: function getAjaxRecords() {
			self = this;
			$.ajax({
				url: "/sources/records",
				type: "POST",
				success: function (s) {
					self.setState({ records: s });
				},
				error: function (xhr, status, error) {
					console.log("xhr: " + xhr.status + ", status: " + status + ", error: " + error);
				}
			});
		}
	}, {
		key: "handleSearch",
		value: function handleSearch(s) {
			this.setState({ records: s });
		}
	}, {
		key: "handleChange",
		value: function handleChange(e) {
			var value = e.target.value;
			var name = e.target.name;
			this.setState(_defineProperty({}, name, value), function () {
				// console.log(this.state[name])
			});
		}
	}, {
		key: "showModal",
		value: function showModal() {
			$("#" + this.modal.id).modal('show');
		}
	}, {
		key: "validate",
		value: function validate(f) {
			var v = true;
			//specific validation adding has-error to div wrapper of element
			//name
			if (f.name == '') {
				$('#d_name').addClass('has-error');
				v = false;
			} else {
				$('#d_name').removeClass('has-error');
			}

			if (f.color == '') {
				$('#d_color').addClass('has-error');
				v = false;
			} else {
				$('#d_color').removeClass('has-error');
			}
			//return validation result
			return v;
		}
	}, {
		key: "save",
		value: function save(callback) {
			self = this;
			var data = {
				name: this.state.name,
				color: this.state.color
			};
			//validate
			if (this.validate(this.state)) {
				$.ajax({
					url: "/sources",
					data: data,
					type: "POST",
					success: function (s) {
						switch (s.status) {
							case "success":
								self.clear();
								self.getAjaxRecords();
								toastr.success("New Source", "Saved");
								swal({
									title: "Saved",
									text: "New Source has been saved",
									type: "success",
									allowEscapeKey: false,
									allowOutsideClick: false
								}, function () {
									// do something after clicking ok of swal
								});
								break;
							case "duplicate":
								toastr.warning(s.message, s.status.toUpperCase());
								// this.refs.btn_save.disabled = false
								break;
						}
					},
					error: function (xhr, status, error) {
						toastr.error("Something Happended", "Error");
						console.log("xhr: " + xhr.status + ", status: " + status + ", error: " + error);
						// this.refs.btn_save.disabled = false
					}
				});
			} else {
					toastr.error('Please Fill Out Required Fields. Labels with *', "Incomplete Fields");
				}
			callback();
		}
	}, {
		key: "update",
		value: function update(id, callback) {
			self = this;
			var data = {
				name: this.state.name,
				color: this.state.color
			};
			if (this.validate(this.state)) {
				$.ajax({
					url: "/sources/" + id,
					data: data,
					type: "patch",
					success: function (s) {
						switch (s.status) {
							case "success":
								self.setState({ mode: "save" });
								self.clear();
								self.getAjaxRecords();
								toastr.success("New Source", "Updated");
								swal({
									title: "Updated",
									text: "Source has been Updated",
									type: "success",
									allowescapekey: false,
									allowoutsideclick: false
								}, function () {
									// do something after clicking ok of swal
								});
								break;
							case "duplicate":
								toastr.warning(s.message, s.status.toUpperCase());
								// this.refs.btn_save.disabled = false
								break;
						}
					},
					error: function (xhr, status, error) {
						toastr.error("something happended", "error");
						console.log("xhr: " + xhr.status + ", status: " + status + ", error: " + error);
						// this.refs.btn_save.disabled = false
					}
				});
			} else {
					toastr.error('please fill out required fields. labels with *', "incomplete fields");
				}
			callback();
		}
	}, {
		key: "new",
		value: function _new() {
			var _this = this;

			this.clear();
			this.setState({ mode: "save" }, function () {
				_this.showModal();
			});
		}
	}, {
		key: "fetch",
		value: function fetch(id, index) {
			var _this2 = this;

			self = this;
			this.clear();
			this.setState({ mode: id }, function () {
				$('input[name="color"]').minicolors('value', { color: _this2.state.records[index].color });
				$('input[name="color"]').val(_this2.state.records[index].color);
				self.setState({
					name: _this2.state.records[index].name,
					color: _this2.state.records[index].color
				}, function () {
					_this2.showModal();
				});
			});
		}
	}, {
		key: "clear",
		value: function clear() {
			this.setState({ name: '', color: '' }, function () {
				$('#d_name').removeClass('has-error');
				$('#d_color').removeClass('has-error');
				$('input[name="color"]').minicolors('value', { color: null });
				$('input[name="color"]').val('');
			});
		}
	}, {
		key: "delete",
		value: function _delete(id, index) {
			self = this;
			var newRecords = this.state.records;
			//confirmation
			swal({
				title: 'Are you sure?',
				text: "You are delete this record!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Delete'
			}).then(function () {
				$.ajax({
					url: "/sources/" + id,
					type: "DELETE",
					success: function (s) {
						switch (s.status) {
							case "success":
								newRecords.splice(index, 1);
								self.setState({ records: newRecords }, function () {
									toastr.info("Record successfully removed", "Deleted");
								});
								break;
						}
					},
					error: function (xhr, status, error) {
						console.log("xhr: " + xhr.status + ", status: " + status + ", error: " + error);
					}
				});
			});
		}
	}, {
		key: "renderMain",
		value: function renderMain() {
			var fields = {
				name: this.state.name
			};
			return React.createElement(
				"div",
				null,
				React.createElement(SourcesForm, { fields: fields,
					change: this.handleChange,
					save: this.save,
					update: this.update,
					modal: this.modal,
					mode: this.state.mode }),
				React.createElement(AddNewButton, { add_new: this["new"] }),
				React.createElement(
					Container,
					null,
					React.createElement(TableSearch, { searched: this.handleSearch,
						url: "sources/search" }),
					React.createElement(Table, { data: this.state.records,
						thead: ['Source Name', 'Color'],
						tdata: [{ column: 'name' }, { column: 'color' }],
						deleteAction: this["delete"],
						editAction: this.fetch,
						classes: "table-hover table-striped" })
				)
			);
		}
	}, {
		key: "render",
		value: function render() {
			return this.renderMain();
		}
	}]);

	return Sources;
})(React.Component);
var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SourcesForm = (function (_React$Component) {
  _inherits(SourcesForm, _React$Component);

  function SourcesForm(props) {
    _classCallCheck(this, SourcesForm);

    _get(Object.getPrototypeOf(SourcesForm.prototype), "constructor", this).call(this, props);
    this.renderMain = this.renderMain.bind(this);
  }

  _createClass(SourcesForm, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      $("input[name='color']").minicolors({ theme: 'bootstrap' });
    }
  }, {
    key: "renderMain",
    value: function renderMain() {
      return React.createElement(
        Modal,
        { modal: this.props.modal },
        React.createElement(
          "form",
          { id: "form_sources" },
          React.createElement(
            "div",
            { className: "row" },
            React.createElement(
              "div",
              { className: "col-sm-12" },
              React.createElement(
                "div",
                { className: "form-group col-sm-7",
                  id: "d_name" },
                React.createElement(
                  "label",
                  { className: "control-label" },
                  "Name",
                  React.createElement(
                    "star",
                    null,
                    "*"
                  )
                ),
                React.createElement("input", {
                  className: "form-control",
                  onChange: this.props.change,
                  value: this.props.fields.name,
                  name: "name",
                  title: "Input Full Name",
                  required: "true" })
              ),
              React.createElement(
                "div",
                { className: "form-group col-sm-5",
                  id: "d_color" },
                React.createElement(
                  "label",
                  { className: "control-label" },
                  "Color"
                ),
                React.createElement("input", {
                  onBlur: this.props.change,
                  value: this.props.fields.color,
                  className: "form-control",
                  name: "color" })
              )
            )
          ),
          React.createElement(ModalSaveUpdate, { mode: this.props.mode,
            update: this.props.update,
            save: this.props.save })
        )
      );
    }
  }, {
    key: "render",
    value: function render() {
      return this.renderMain();
    }
  }]);

  return SourcesForm;
})(React.Component);
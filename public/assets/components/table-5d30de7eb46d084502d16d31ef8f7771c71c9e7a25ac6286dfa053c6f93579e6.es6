var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Table = (function (_React$Component) {
  _inherits(Table, _React$Component);

  function Table(props) {
    _classCallCheck(this, Table);

    _get(Object.getPrototypeOf(Table.prototype), "constructor", this).call(this, props);
    this.renderMain = this.renderMain.bind(this);
    this.renderTableData = this.renderTableData.bind(this);
    this.tableAction = this.tableAction.bind(this);
  }

  _createClass(Table, [{
    key: "tableAction",
    value: function tableAction(row, i) {
      switch (this.props.taction) {
        case 'reservation_records':
          return React.createElement(ReservationRecordsRow, { key: i,
            record: row,
            checkin: this.props.checkin,
            checkout: this.props.checkout,
            receipt: this.props.receipt,
            index: i });
          break;
        default:
          return React.createElement(Actions, { index: i, id: row.id,
            editAction: this.props.editAction,
            deleteAction: this.props.deleteAction });
      }
    }
  }, {
    key: "renderTableData",
    value: function renderTableData(row, i) {
      var _this = this;

      return React.createElement(
        "tr",
        { key: i },
        React.createElement(
          "td",
          null,
          i + 1
        ),
        this.props.tdata.map(function (data, i) {
          return React.createElement(
            "td",
            { key: i },
            _this.processData(row, data)
          );
        }),
        React.createElement(
          "td",
          null,
          this.tableAction(row, i)
        )
      );
    }
  }, {
    key: "processData",
    value: function processData(row, data) {
      if (row[data.column]) {
        if (data.transform) {
          return uc.transform[data.transform](row, row[data.column]);
        } else {
          return row[data.column];
        }
      }
    }
  }, {
    key: "emptyTable",
    value: function emptyTable() {
      var length = $("table > thead > tr:first > th").length;
      return React.createElement(
        "tr",
        null,
        React.createElement(
          "td",
          { colSpan: length },
          React.createElement(
            "div",
            { className: "text-center" },
            React.createElement(
              "h5",
              null,
              "No Record"
            ),
            React.createElement(Img, {
              className: "no-record",
              src: "/assets/search2.gif" })
          )
        )
      );
    }
  }, {
    key: "renderMain",
    value: function renderMain() {
      var data = this.props.data;
      return React.createElement(
        "table",
        { id: "#table", className: "table sortable " + this.props.classes },
        React.createElement(
          "thead",
          null,
          React.createElement(
            "tr",
            null,
            React.createElement(
              "th",
              null,
              "#"
            ),
            this.props.thead.map(function (value, i) {
              return React.createElement(
                "th",
                { key: i },
                value
              );
            }),
            React.createElement(
              "th",
              { "data-defaultsort": "disabled" },
              "Actions"
            )
          )
        ),
        React.createElement(
          "tbody",
          null,
          data.length == 0 ? this.emptyTable() : this.props.data.map(this.renderTableData)
        )
      );
    }
  }, {
    key: "render",
    value: function render() {
      return this.renderMain();
    }
  }]);

  return Table;
})(React.Component);
var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TableSearch = (function (_React$Component) {
  _inherits(TableSearch, _React$Component);

  function TableSearch(props) {
    _classCallCheck(this, TableSearch);

    _get(Object.getPrototypeOf(TableSearch.prototype), "constructor", this).call(this, props);
    this.renderMain = this.renderMain.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  /*
    Use Component before table
    In parent component , create a function that will replace
    the current state with the returned records of this Component like this:
  
  		<TableSearch searched={this.handleSearch}
  			url="clients/search"/>
  
    In Prent component, add props like this:
  
    	<TableSearch searched={this.handleSearch}/>
  */

  _createClass(TableSearch, [{
    key: "handleChange",
    value: function handleChange(e) {
      self = this;
      $.ajax({
        url: this.props.url,
        data: { search: e.target.value },
        type: "POST",
        success: function (s) {
          self.props.searched(s);
        },
        error: function (xhr, status, error) {
          console.log("xhr: " + xhr.status + ", status: " + status + ", error: " + error);
        }
      });
    }
  }, {
    key: "renderMain",
    value: function renderMain() {
      return React.createElement(
        "div",
        { className: "row" },
        React.createElement(
          "div",
          { className: "col-sm-3 col-xs-12 form-group" },
          React.createElement("input", { className: "form-control",
            onChange: this.handleChange,
            placeholder: "Search" })
        ),
        this.props.children
      );
    }
  }, {
    key: "render",
    value: function render() {
      return this.renderMain();
    }
  }]);

  return TableSearch;
})(React.Component);
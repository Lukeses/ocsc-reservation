
var bookings_index = {


  clear_calendar : function(){
  	$('#reservation-body').empty();
  	$('#reservation-header').empty();
  },
  //Current Month Calendar Counter
  fill_calendar: function(mx, dx, yx){
    dx = null
    this.clear_calendar()
    //Set Month Label
    var monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"];

    var x = document.createElement("TH");
    x.innerHTML += monthNames[parseInt(mx-1, 10)] + " " +yx;
    document.getElementById('reservation-header').appendChild(x);

    function daysInThisMonth() {
      var now = new Date();
      return new Date(yx, mx, 0).getDate();
    }

    for(i=1; i<=daysInThisMonth(); i++){
    	//header
    	var x = document.createElement("TH");
    	x.innerHTML += i;
    	document.getElementById('reservation-header').appendChild(x);
    }

    for(y=1; y<5; y++){
       //Property Name
       var tr = document.createElement('tr');
       var td = document.createElement("td"); td.style.maxWidth = "100%";

       td.innerHTML += "Property #";
       td.innerHTML += y;

       tr.appendChild(td);

        for(i=1; i<=daysInThisMonth(); i++){

        	//body - Insert availability per date here. Color indicates availability
        	var td = document.createElement("td");
        	td.id = "pid_"+y + "-" + mx + "-"+i  + "-" + yx;
        	td.classList.add('success');
          tr.appendChild(td);

          //On Click Function for each cell
          td.onclick = function(){
            swal(this.id)
          // this.className = "info"; $("#myModal").modal();
          };
        }//end for loop
      	document.getElementById('reservation-body').appendChild(tr);

    }//end for loop

  }, //end fill_calendar

  at_load : function(){
    ito = bookings_index
    uc.misc.initFormExtendedDatetimepickers()
    var d = new Date();
    ito.fill_calendar(d.getMonth()+1, d.getDay(), d.getFullYear());
        //
  },
  at_init : function() {
    uc.sidebar.menu_name1(`bookings`)
    // uc.misc.initFormExtendedDatetimepickers()
  },
  at_dom : function() {
    ito = bookings_index
    $('#date-picker-1').on('dp.change', function(event) {
      let formatted_month = event.date.format('MM');
      let formatted_day = event.date.format('DD');
      let formatted_year = event.date.format('YYYY');
      //ito.fill_calendar(formatted_month, formatted_day, formatted_year);
    });

    $('#date-picker-1').on('dp.hide', function(event){
      setTimeout(function(){
        $(`#date-picker-1`).data('DateTimePicker').viewMode('months');
      },1);
    });

  },

}

$(document).ready(function(){
  ito  = bookings_index
  //ito.at_load()
  //ito.at_init()
  //ito.at_dom()
})
;
